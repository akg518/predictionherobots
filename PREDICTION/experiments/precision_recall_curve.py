import matplotlib.pyplot as plt
import numpy as np
from imblearn.over_sampling import SMOTE
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import precision_recall_curve
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import LabelEncoder
from collections import Counter

from PREDICTION.loading.xy_loader import load_XY

processed_file = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/processed.csv'
raw_file = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/raw.csv'
X, y = load_XY(raw_file, processed_file)
le = LabelEncoder()
y_twoclass = le.fit_transform(y == 'Steady')
X_train, X_test, y_train, y_test = train_test_split(
        X, y_twoclass, test_size=0.2, shuffle=False)

print("oversampling steady label")
sampler = SMOTE()
X_train, y_train = sampler.fit_resample(X_train, y_train)

print("training model...")
params = {'n_estimators': np.linspace(20, 400, 10, dtype=np.int), 'max_features': np.linspace(7, 70, 5, dtype=np.int)}
predictor = RandomForestRegressor(n_estimators=100)
regressor = GridSearchCV(predictor, params, cv=3, verbose=5, n_jobs=3)
regressor.fit(X_train, y_train)
print("model trained!")

precision, recall, _ = precision_recall_curve(y_train, regressor.predict(X_train), pos_label=1)

plt.step(recall, precision, color='b', alpha=0.2,
         where='post')
plt.fill_between(recall, precision, alpha=0.2, color='b', step='post')

plt.xlabel('Recall')
plt.ylabel('Precision')
plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.title('2-class Precision-Recall curve:')
plt.show()