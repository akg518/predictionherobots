import pandas as pd
from sklearn.svm import SVC

from PREDICTION.loading import xy_loader
import numpy as np
from scipy.spatial import distance

processed = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/processed.csv'
raw = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/raw.csv'

X, y = xy_loader.load_XY(raw, processed)

X, y = X[-1000:], y[-1000:]

D = distance.pdist(X)

svc = SVC(C=8, gamma=2)

results = pd.DataFrame()
results['y'] = y




