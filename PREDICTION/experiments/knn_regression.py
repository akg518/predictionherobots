import matplotlib.pyplot as plt
import numpy as np

sample_size = 100
coverage_ratio = 0.1
coverage_samples = 10
pixel_res = 100
n_neighbors = 20
dim = 2

# generate good points
good_x = np.random.multivariate_normal((3, 3), [[2.5, 0], [0, 2.5]], sample_size)
bad_x = np.random.multivariate_normal((1, 1), [[1, 0], [0, 1]], sample_size)
data = np.concatenate((bad_x, good_x), axis=0)
y = np.concatenate((np.zeros((sample_size, 1)), np.ones((sample_size, 1))), axis=0)


def get_coverage_window(X, coverage_ratio):
    return (np.max(X, axis=0) - np.min(X, axis=0)) * coverage_ratio


def get_coverage(coverage_window, data, X, no_coverage_samples=10):
    lower_bounds = X - coverage_window / 2
    upper_bounds = X + coverage_window / 2
    no_in_bounds = sum(np.all((lower_bounds <= data) & (data < upper_bounds), axis=1))
    return min(no_in_bounds, no_coverage_samples) / no_coverage_samples


upper_bounds = np.max(data, axis=0) + 1
lower_bounds = np.min(data, axis=0) - 1

coverage_window = get_coverage_window(data, coverage_ratio)
coords = np.stack(
    np.meshgrid(*[np.linspace(lower_bounds[i], upper_bounds[i], pixel_res) for i in range(dim)], indexing='xy'),
    axis=-1)
coverage = lambda X: get_coverage(coverage_window, data, X)
pixels = np.apply_along_axis(coverage, -1, coords)

plt.imshow(pixels, extent=(lower_bounds[0], upper_bounds[0], lower_bounds[1], upper_bounds[1]), origin='lower', interpolation='gaussian')
plt.scatter(data[:, 0], data[:, 1])
plt.show()
