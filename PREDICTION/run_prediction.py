"""running utility, this is meant to be ran every hour"""
import logging
from datetime import datetime, timedelta
from sys import exit

import pytz
from slacker_log_handler import SlackerLogHandler, NoStacktraceFormatter

from PREDICTION.loading import database_connect
from PREDICTION.prediction import predict_svc

BTC_PREDICTION_HOURS = range(0, 24, 2)
ETH_PREDICTION_HOURS = range(1, 24, 2)

USE_LOCALE = 'Singapore'

MINUTE_PREDICTION_RANGE = (31, 34)

# formatting setup
slack_handler = SlackerLogHandler('xoxp-537340929715-537340930979-538023193397-5180882d39a7f7b45d8d5e73881bf994',
                                  '#log', stack_trace=True, icon_emoji=':robot_face:', ping_level=logging.error)
logger = logging.Logger('PredictionHerobots', level=logging.DEBUG)
logger.addHandler(slack_handler)
formatter = NoStacktraceFormatter('%(asctime)s - %(filename)s - %(levelname)s:%(message)s')
slack_handler.setFormatter(formatter)
slack_handler.setLevel(logging.DEBUG)
try:
    current_utc_time = datetime.utcnow().replace(tzinfo=pytz.utc)
    current_local_time = current_utc_time.astimezone(pytz.timezone(USE_LOCALE))
    prediction_time = current_utc_time.replace(hour=(current_utc_time.hour + 2) % 24, minute=0, second=0,
                                                 microsecond=0)
    predictor = predict_svc.SVCHourPredictor()

    if current_utc_time.minute < MINUTE_PREDICTION_RANGE[0] or current_utc_time.minute > MINUTE_PREDICTION_RANGE[1]:
        logger.error("script called at an incorrect minute range, quitting now...")
        #exit(1)

    logger.info("attempting to connect to the database...")
    db = database_connect.DatabaseConnect(model_description="SVC")

    logger.info("predicting BTC prices...")
    prediction_result = predictor.predict_BTC(utc_time=current_utc_time)
    logger.info("predicted label is: *{}*".format(prediction_result))
    logger.info("pushing predictions to the database...")
    db.insert_predictions('BTC', prediction_result, prediction_time)
    logger.info("predicting ETH prices")
    prediction_result = predictor.predict_ETH(utc_time=current_utc_time)
    logger.info("predicted label is: *{}*".format(prediction_result))
    logger.info("pushing predictions to the database...")
    db.insert_predictions('ETH', prediction_result, prediction_time)
    
except Exception:
    logger.exception("Unhandled exception has been raised!")
    raise
