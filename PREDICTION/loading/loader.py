from datetime import timedelta, datetime
from functools import partial

import pandas as pd

from PREDICTION.loading.config import *
from PREDICTION.loading.featureprocessor import FeatureProcessor
from PREDICTION.loading.poller import PollCryptoCompare


class Loader:
    """
    Make a big dictionary of call types, use these to poll.
    """

    def __init__(self, use_ticker = None):
        self.DATA_DIR = DATA_DIR
        self.sparevar = None
        if use_ticker is None:
            self.tickers = ['ETH', 'BTC']
        elif use_ticker in ['ETH', 'BTC']:
            self.tickers = [use_ticker]
        else:
            raise ValueError("Invalid ticker provided!")
        self.polls = ['ohlc', 'social']
        self.compare_against = ['USD']
        self.exchanges = ['CCCAGG']
        self.times = ['minute', 'hour']
        if SKIP_SOCIAL:
            self.polls = ['ohlc']
        if SKIP_MINUTE:
            self.times = ['hour']
        if DEBUG_LOADER:
            self.tickers = ['ETH']
            self.polls = ['ohlc']
            self.times = ['hour']

        self.chosen_sources = {'tickers': self.tickers, 'polls': self.polls, 'compare_against': self.compare_against,
                               'exchanges': self.exchanges, 'times': self.times}

        self.biggus_dictus = self.dict_of_dat(self.chosen_sources)  # defaultdict()

    def dict_of_dat(self, sources):
        dict_for_all = {}
        for ticker in sources['tickers']:
            for poll in sources['polls']:
                if poll == 'social':
                    id = ticker + poll  # + against + exchange
                    dir = self.DATA_DIR + ticker + '/' + poll + '/'  # + against + '/'+ exchange  + '/'  + time + '/'
                    labels = {'ticker': ticker, 'poll': poll, 'against': None, 'exchange': None, 'time_unit': 'minute'}
                    dict_for_all[id] = {'dir': dir, 'labels': labels}

                else:
                    for against in sources['compare_against']:
                        for exchange in sources['exchanges']:
                            for time in sources['times']:
                                id = ticker + poll + against + exchange + time
                                dir = self.DATA_DIR + ticker + '/' + poll + '/' + against + '/' + exchange + '/' + time + '/'
                                labels = {'ticker': ticker, 'poll': poll, 'against': against, 'exchange': exchange,
                                          'time_unit': time}
                                dict_for_all[id] = {'dir': dir, 'labels': labels}
        return dict_for_all

    def two_frames(self, a, b):
        return pd.merge(a, b, how='outer', left_index=True, right_index=True)

    def combine(self, name, index):

        self.sparevar.append(index['output'])
        return None, ''

    def flatten_data(self, t_dict):
        self.sparevar = []
        self.run_on_multiple_sources(self.combine, t_dict)
        return pd.concat(self.sparevar, sort=True)

    def to_file(self, filename, name, index):
        index['output'].to_csv(index['dir'] + filename)
        return None, ''

    def run_on_multiple_sources(self, method, sources=None, return_obj=False):
        """
        Runs a specified method across a selection of sources.
        :param method: method to be called. Method signature must be of type (source, attributes)
        :param sources: the sources to be used. If not specified all sources will be used.
        :param return_obj: TODO can you please tell me what this is for? I think this was to do with trying to
        concatenate all frames - I was trying to figure a way of having a variable accessible by all the methods.

        :return: a dictionary of per source method calculations
        """
        if sources is None:
            sources = self.biggus_dictus
        if return_obj:
            return_obj.spare_var = 0
        applied_dict = {}
        for source, attributes in sources.items():
            print(source, len(attributes))
            output, suffix = method(source, attributes)
            applied_dict[source + suffix] = {'dir': attributes['dir'], 'labels': attributes['labels'], 'output': output}
        return applied_dict

    def price_aggregate(self, name, index):
        if 1:  # index['labels']['polls']=='ohlc':
            f = index['output'].copy().reset_index()
            f['Date'] = pd.to_datetime(f['time'], unit='s')
            f.set_index('Date', inplace=True)
            f.sort_index(inplace=True)

            def take_first(array_like):
                return array_like[0]

            def take_last(array_like):
                return array_like[-1]

            ohlc_how = {'open': take_first,
                        'high': 'max',
                        'low': 'min',
                        'close': take_last,
                        'volumefrom': 'sum',
                        'volumeto': 'sum'}
            dt = timedelta(minutes=datetime.now().minute)
            aggregated = f.resample('1h', loffset=dt).agg(ohlc_how, label='left')[
                ['open', 'close', 'high', 'low', 'volumefrom', 'volumeto']]
            a = aggregated.reset_index()
            a['time'] = a['Date'].apply(lambda v: v.timestamp())
            a = a.set_index('time').drop(columns=['Date'])
            aggregated.to_csv(index['dir'] + 'one_hour_agg_raw.csv')
            return aggregated, ''


def big_plot(name, index):
    print(name)
    index['output'].plot()


def load_and_preprocess(use_stable_version=True, use_ticker=None):
    loader = Loader(use_ticker)

    Poller = PollCryptoCompare()
    raw_dict = loader.run_on_multiple_sources(Poller.poll_historical)

    feature_processor = FeatureProcessor()
    if use_stable_version:
        features = loader.run_on_multiple_sources(feature_processor.generate_features_stable, raw_dict)
        loader.run_on_multiple_sources(partial(loader.to_file, 'processed.csv'), features)
    else:
        features = loader.run_on_multiple_sources(feature_processor.generate_features_experimental, raw_dict)
        loader.run_on_multiple_sources(partial(loader.to_file, 'processed.csv'), features)
        patterns = loader.run_on_multiple_sources(feature_processor.gen_patterns, raw_dict)
        loader.run_on_multiple_sources(partial(loader.to_file, 'patterns.csv'), patterns)
    targets = loader.run_on_multiple_sources(feature_processor.targets, raw_dict)

    for i, v in targets.items():
        if v['labels']['poll'] == 'ohlc':
            temp_yf = v['output'].reset_index()
            temp_yf.to_csv(v['dir'] + 'targets_processed.csv')

    if not SKIP_AGGREGATING:
        agg = loader.run_on_multiple_sources(loader.price_aggregate, raw_dict)
        if use_stable_version:
            agg_normal = loader.run_on_multiple_sources(feature_processor.generate_features_stable, agg)
        else:
            agg_normal = loader.run_on_multiple_sources(feature_processor.generate_features_experimental, agg)
        agg_targets = loader.run_on_multiple_sources(feature_processor.targets, agg)
        for i, v in agg_normal.items():
            if v['labels']['poll'] == 'ohlc':
                temp_aggxf = v['output']
                temp_aggxf.to_csv(v['dir'] + 'one_hour_agg_processed.csv')

    return features, targets