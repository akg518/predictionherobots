import math
import os
import pickle
from multiprocessing import Pool

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler

from PREDICTION.loading.config import *

if USE_TALIB:
    import talib
    from talib import abstract


def _calc_vap_bucket(raw_df, total_vol, lower_bucket_limit, upper_bucket_limit):
    print("calculating for bucket ({0:.3f}, {1:.3f})".format(lower_bucket_limit, upper_bucket_limit))
    return raw_df['volumefrom'] \
               .rolling(VAP_RANGE) \
               .apply(FeatureProcessor._get_volume_at, args=(raw_df, lower_bucket_limit, upper_bucket_limit),
                      raw=False) \
           / total_vol


class FeatureProcessor:
    """
    Handles feature generation
    """

    def SMA(self, df, window_size):
        return df.rolling(window_size).mean()

    def perc_dif(self, x, against):
        return np.divide(np.subtract(x, against), against + 1)

    def targets(self, name, index):
        labels = index['labels']
        if labels['poll'] == 'ohlc':
            df = index['output'].copy()
            df = df[~df.index.duplicated(keep='first')]
            df2 = pd.DataFrame(index=df.index)
            targets = pd.DataFrame(index=df.index)

            features = {}
            unit = TIME_UNIT_SECONDS[labels['time_unit']]

            targets = self.perc_dif(df.shift(-2 * math.ceil(3600.0 / unit))['close'], df['close'])

            for window in SMA_WINDOWS:
                # normalise_against[index][ticker + poll + index + ' against' + ' SMA' + str(window)] = self.SMA(X,
                # features['against_SMA' + str(window)] = self.perc_dif(X, self.SMA(X, window))  # window)
                pass
            first = True
            for name, feature in features.items():
                df2 = pd.merge(df2, feature.add_suffix('_' + name), how='outer', left_index=True,
                               right_index=True)  # , suffixes=('', '_' + name))

            output = df2
            suffix = ''
            return targets, suffix
        else:
            return None, ''

    def _gen_SMA_features(self, raw_df):
        """
        generates SMA features. Internal function.
        :param raw_df: clean raw signals dataframe
        :return: Dataframe containing SMA features.
        """
        result = pd.DataFrame(index=raw_df.index)
        SMA_features = {}
        for window in SMA_WINDOWS:
            SMA_features['against_SMA%s' % window] = self.perc_dif(raw_df, self.SMA(raw_df, window))

        for name, feature in SMA_features.items():
            result = pd.merge(result, feature.add_suffix('name' + name), how='outer', left_index=True,
                              right_index=True)
        return result

    def _gen_EMA_features(self, raw_df: pd.DataFrame):
        result = pd.DataFrame(index=raw_df.index)
        EMA_features = {}
        for halflife in EMA_HALFLIFES:
            ewm_mean = raw_df.ewm(halflife=halflife, min_periods=halflife).mean()
            EMA_features['_EMA_%s' % halflife] = self.perc_dif(raw_df, ewm_mean)
        for name, feature in EMA_features.items():
            result = pd.merge(result, feature.add_suffix(name), how='outer', left_index=True,
                              right_index=True)
        return result

    def _gen_volatility_features(self, raw_df):
        """
        Geneartes volatility features. Calculated based on the window specified in config.
        Note: it calculates %ge dif volatility instead of raw volatility to make it stationary.
        :param raw_df: clean raw singals dataframe
        :type raw_df: pd.DataFrame
        :return: Dataframe containing volatility features
        """
        result = pd.DataFrame(index=raw_df.index)
        percentage_differences = self.perc_dif(raw_df['close'], raw_df['close'].shift(1))
        for window in VOLATILITY_WINDOWS:
            result['{}H_volatility'.format(window)] = percentage_differences.rolling(window).std()
        return result

    @staticmethod
    def _get_volume_at(rolling_volume, raw_df, lower_bound, upper_bound):
        prices = raw_df.loc[rolling_volume.index, 'open']
        current_price = prices.iloc[-1]
        price_lower_bound = current_price + current_price * lower_bound
        price_upper_bound = current_price + current_price * upper_bound
        prices_in_bound = prices[(price_lower_bound <= prices) & (prices <= price_upper_bound)]
        return sum(rolling_volume[prices_in_bound.index])

    def _gen_VAP_features(self, raw_df: pd.DataFrame):
        """
        Generates volume at price statistics, as described by the initial VAP parameters.
        :param raw_df: the raw dataframe with cleaned raw signals.
        :return: loader pandas dataframe with VAP specific data
        """
        print("calculating vap features")
        result = pd.DataFrame(index=raw_df.index)
        result['{}H_total_vol'.format(VAP_RANGE)] = raw_df['volumefrom'].rolling(VAP_RANGE).sum()
        vap_total_range = NO_VAP_BUCKETS * VAP_BUCKET_SIZE
        vap_midpoints = np.linspace(0, vap_total_range, NO_VAP_BUCKETS) - vap_total_range / 2
        vap_buckets = [(midpoint - VAP_BUCKET_SIZE / 2, midpoint + VAP_BUCKET_SIZE / 2) for midpoint in vap_midpoints]
        p = Pool(POOL_WORKERS)
        intermediary_results = \
            p.starmap(_calc_vap_bucket, ((raw_df, result['{}H_total_vol'.format(VAP_RANGE)], bucket[0], bucket[1])
                                         for bucket in vap_buckets))
        for ix in range(len(vap_buckets)):
            result['volume@({0:.3f}, {1:.3f})%'.format(vap_buckets[ix][0], vap_buckets[ix][1])] = intermediary_results[
                ix]

        return result

    def _gen_minmax_features(self, raw_df):
        """
        Generates minimum and maximum features for specified time windows. Features are price normalised.
        :param raw_df: clean raw singals dataframe
        :type raw_df: pd.DataFrame
        :return: Dataframe containing min and max features
        """
        result = pd.DataFrame(index=raw_df.index)
        for window in MINMAX_WINDOWS:
            prices = (raw_df['close'] + raw_df['open']) / 2
            result['{}H_min'.format(window)] = raw_df['high'].rolling(window).min() / prices
            result['{}H_max'.format(window)] = raw_df['high'].rolling(window).max() / prices
        return result

    def _gen_indicators(self, raw_df):
        raw_df = raw_df.rename(columns={'volumefrom': 'volume'})
        result = pd.DataFrame(index=raw_df.index)
        for timespan in [5, 10, 24]:
            result['adx_{}'.format(timespan)] = abstract.ADX(raw_df, timeperiod=timespan)
            result['mfi_{}'.format(timespan)] = abstract.MFI(raw_df, timeperiod=timespan)
            result['rsi_{}'.format(timespan)] = abstract.RSI(raw_df, timeperiod=timespan)

        # TODO ADD MORE INDICATORS
        return result

    def generate_features_stable(self, dataset_name, dataset_attributes):
        """
            Generates features from raw inputs. Stable version used for production environment.
            :param dataset_name: descriptive, usually ignored
            :param dataset_attributes: dictionary containing the signals and some other data
            :return: tuple of (feature_dataframe, ''), again empty used for standardisation
        """
        df = dataset_attributes['output'].copy()
        df = df[~df.index.duplicated(keep='first')]

        for signal in RAW_BLACKLIST:
            if signal in df:
                del df[signal]

        if not isinstance(df.index, pd.DatetimeIndex):  # only filter numerical timestamps
            df = df[df.index > GEN_FEATURES_FROM_TIME]  # removing shit data TODO generalise this
        result = pd.DataFrame(index=df.index)

        if SKIP_SOCIAL and 'social' in dataset_name:
            return result, ''

        if GENERATE_SMA_FEATURES:
            result = pd.merge(result, self._gen_SMA_features(df), how='outer', left_index=True, right_index=True)
        if GENERATE_EMA_FEATURES:
            result = pd.merge(result, self._gen_EMA_features(df), how='outer', left_index=True, right_index=True)
        if GENERATE_VOLATILITY_FEATURES and 'close' in df:
            result = pd.merge(result, self._gen_volatility_features(df), how='outer', left_index=True, right_index=True)
        if GENERATE_VAP_FEATURES:
            result = pd.merge(result, self._gen_VAP_features(df), how='outer', left_index=True, right_index=True)
        if GENERATE_MINMAX_FEATURES:
            result = pd.merge(result, self._gen_minmax_features(df), how='outer', left_index=True, right_index=True)

        if USE_TALIB:
            result = pd.merge(result, self._gen_indicators(df), how='outer', left_index=True, right_index=True)

        # drop nan samples
        result = result.dropna()

        # scale the features
        if SCALE_FEATURES:
            if not USE_CURRENT_SCALER:  # create a new scaler value
                scaler = StandardScaler()
                scaler.fit(result.values)
                with open(os.path.join(dataset_attributes['dir'], 'feature_scaler.pkl'), 'wb') as f:
                    pickle.dump(scaler, f)
            else:  # use a preexisting scaler value
                with open(os.path.join(dataset_attributes['dir'], 'feature_scaler.pkl'), 'rb') as f:
                    scaler = pickle.load(f)

            result[result.columns] = scaler.transform(result.values)

        return result, ''

    def generate_features_experimental(self, dataset_name, dataset_attributes):
        """
        Generates features from raw inputs. Experimental version used for testing new features
        :param dataset_name: descriptive, usually ignored
        :param dataset_attributes: dictionary containing the signals and some other data
        :return: tuple of (feature_dataframe, ''), again empty used for standardisation
        """
        df = dataset_attributes['output'].copy()
        df = df[~df.index.duplicated(keep='first')]

        for signal in RAW_BLACKLIST:
            if signal in df:
                del df[signal]

        if not isinstance(df.index, pd.DatetimeIndex):  # only filter numerical timestamps
            df = df[df.index > GEN_FEATURES_FROM_TIME]  # removing shit data TODO generalise this
        result = pd.DataFrame(index=df.index)

        if SKIP_SOCIAL and 'social' in dataset_name:
            return result, ''

        if GENERATE_SMA_FEATURES:
            result = pd.merge(result, self._gen_SMA_features(df), how='outer', left_index=True, right_index=True)
        if GENERATE_EMA_FEATURES:
            result = pd.merge(result, self._gen_EMA_features(df), how='outer', left_index=True, right_index=True)
        if GENERATE_VOLATILITY_FEATURES and 'close' in df:
            result = pd.merge(result, self._gen_volatility_features(df), how='outer', left_index=True, right_index=True)
        if GENERATE_VAP_FEATURES:
            result = pd.merge(result, self._gen_VAP_features(df), how='outer', left_index=True, right_index=True)
        if GENERATE_MINMAX_FEATURES:
            result = pd.merge(result, self._gen_minmax_features(df), how='outer', left_index=True, right_index=True)

        if USE_TALIB:
            result = pd.merge(result, self._gen_indicators(df), how='outer', left_index=True, right_index=True)

        # drop nan samples
        result = result.dropna()

        # scale the features
        if SCALE_FEATURES:
            if not USE_CURRENT_SCALER:  # create a new scaler value
                scaler = StandardScaler()
                scaler.fit(result.values)
                with open(os.path.join(dataset_attributes['dir'], 'feature_scaler.pkl'), 'wb') as f:
                    pickle.dump(scaler, f)
            else:  # use a preexisting scaler value
                with open(os.path.join(dataset_attributes['dir'], 'feature_scaler.pkl'), 'rb') as f:
                    scaler = pickle.load(f)

            result[result.columns] = scaler.transform(result.values)

        return result, ''

    def gen_patterns(self, dataset_name, dataset_attributes):
        """
        Generates candlestick pattern data.
        :param dataset_name: The dataset name.
        :param dataset_attributes: The dataset and extra attributes.
        :return: Pattern Dataframe.
        """

        df = dataset_attributes['output'].copy()
        df = df[~df.index.duplicated(keep='first')]

        min_pattern_recognitions = 0.01 * df.shape[0]

        if not isinstance(df.index, pd.DatetimeIndex):  # only filter numerical timestamps
            df = df[df.index > 1450000000]  # removing shit data TODO generalise this
        result = pd.DataFrame(index=df.index)

        for pattern in talib.get_function_groups()['Pattern Recognition']:
            result[pattern] = getattr(talib, pattern)(df['open'], df['high'], df['low'], df['close'])

        # remove bad patterns
        for col in result.columns:
            if sum(result[col] != 0) < min_pattern_recognitions:
                del result[col]
        return result / 100, ''
