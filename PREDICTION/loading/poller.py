import os
import pandas as pd
import numpy as np
import requests
import time
import math

from PREDICTION.loading.config import *


def call_cc(url):
    print('CALLING', url)
    f = requests.get(url)
    print(url, f.json())
    return f.json()


class PollCryptoCompare:
    """
    Class for updating data from CryptoCompare
    """

    def __init__(self):
        self.my_key = "ec30f776280327f11539d18050600761a332070c6a37f4062fa7123467843b2d"
        if not os.path.isfile(DATA_DIR + 'misc/coin_data.npy'):
            self.coin_data = call_cc("https://min-api.cryptocompare.com/data/all/coinlist")['Data']
            if not os.path.isdir(DATA_DIR + 'misc/'):
                os.makedirs(DATA_DIR + 'misc')
            np.save(DATA_DIR + 'misc/coin_data.npy', self.coin_data)
        else:
            self.coin_data = np.load(DATA_DIR + 'misc/coin_data.npy').item()


    def price_url(self, args):
        return "https://min-api.cryptocompare.com/data/histo{0}?fsym={1}&tsym={2}&e={3}". \
            format(args['time_unit'], args['ticker'], args['against'], args['exchange'])

    def social_url(self, args):
        return "https://min-api.cryptocompare.com/data/social/coin/histo/hour?coinId={0}&api_key={1}".format(
            self.coin_data[args['ticker']]['Id'], self.my_key)

    def poll_historical(self, name, index):
        print(name, index)
        labels = index['labels']
        out_dir = index['dir']

        interval = 60 ** 2

        if labels['poll'] == 'social':
            base_url = self.social_url(labels)
        else:
            base_url = self.price_url(labels)
            if labels['time_unit'] == 'minute':
                interval = 60

        is_data = False
        csv_path = out_dir + 'raw.csv'
        isfile = os.path.isfile(csv_path)

        if isfile:
            old_df = pd.read_csv(csv_path)
            print(old_df.index.name)
            is_data = not old_df.empty

        if is_data:
            old_df = pd.read_csv(csv_path)

            print(old_df.index.name)

            if old_df.index.name != 'time':
                old_df = old_df.set_index('time')
            old_df = old_df.sort_index()
            minutes_to_grab = int((time.time() - old_df.index.values[-1]) / interval)
            print((time.time() - old_df.index.values[-1]) / interval, minutes_to_grab, old_df.index.values[-1])
        else:
            print('old csv file not found, pulling for the whole range...')
            if SKIP_PULLING:
                raise ValueError("cannot find previous data, please set SKIP_PULLING to False")
            old_df = pd.DataFrame
            try:
                os.makedirs(out_dir)
            except OSError as e:
                print(e)
            minutes_to_grab = 999999999999
        url = base_url

        grabbing = int(min(minutes_to_grab, 2000))
        we_grabbing = int(minutes_to_grab) > 0
        if not SKIP_PULLING:
            if we_grabbing:
                ipdata = call_cc(url + "&limit={0}".format(grabbing))
                minutes_to_grab -= grabbing
                df = pd.DataFrame(ipdata['Data'])
            if we_grabbing and (not df.empty):
                df = df.set_index('time')
                df = df.sort_index()
                for i in range(math.ceil(minutes_to_grab / 2000)):
                    grabbing = int(min(minutes_to_grab, 2000))
                    url = base_url + "&limit={0}&toTs={1}".format(grabbing, df.iloc[0].name)
                    ipdata = call_cc(url)
                    tp = pd.DataFrame(ipdata['Data'])
                    minutes_to_grab -= grabbing

                    print(tp)

                    # return df_processed
                    if tp.empty:
                        break
                    tp = tp.set_index('time')
                    if (np.mean(tp) == 0).all():
                        break
                    df = pd.concat([df, tp])
                    df = df.sort_index()
        if is_data:
            if not SKIP_PULLING and we_grabbing:
                df = pd.concat([old_df, df])
            else:
                df = old_df
        df.sort_index()
        df = np.trim_zeros(df)

        df.to_csv(csv_path)

        return df, ''
