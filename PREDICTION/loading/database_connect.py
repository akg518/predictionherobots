from datetime import datetime

import pandas as pd
import pytz
from pymongo import MongoClient


class DatabaseConnect:
    def __init__(self, model_description="ensemble"):
        username = "dudes"
        password = "dudes34"
        self.Client = MongoClient(host="13.125.150.105", port=27017, username=username, password=password,
                                  authsource='Just_two_dudes')
        self.DB_name = self.Client['Just_two_dudes']
        self.Collections = {'ETH': self.DB_name['ETH_results'], 'BTC': self.DB_name['BTC_results']}
        self.team_name = "Just_two_dudes"
        self.model_nick = "zaphod"  # "gary"#
        self.model_description = model_description
        self.target_timezone = pytz.timezone('Singapore')
        self.time_format = '%Y-%m-%d %H:%M:%S'

        self.live = False

    def default_time(self):
        t = datetime.now()
        t = t.replace(microsecond=0, second=0, minute=0, hour=t.hour + 2)
        return t

    def load_data(self):
        """ Returns ETH and BTC minute dataframes in a dictionary"""
        ETH_dataframe = pd.DataFrame(list(self.Client['BINANCE']['ETH_USD_1MIN'].find()))
        BTC_dataframe = pd.DataFrame(list(self.Client['BINANCE']['BTC_USD_1MIN'].find()))

        return {"ETH": ETH_dataframe, "BTC": BTC_dataframe}

    def insert_predictions(self, ticker, forecast_label, prediction_time=None, convert_time=True):
        """
        Pushes predictions to the PredictionHerobots database
        :param ticker: ETH/BTC
        :param forecast_label: the predicted bucket (Rise/Steady/Fall)
        :param prediction_time: Datetime object, is converted into SG time before push. If left blank we use 2 hours
         after the next full hour.
        :param convert_time: whether to convert the time to Korean Standard time
        """

        if prediction_time is None:
            prediction_time = self.default_time()
        if convert_time:
            prediction_time = prediction_time.astimezone(self.target_timezone).strftime(self.time_format)

        payload = {"H_nick_name": self.model_nick, " H_model_name": self.team_name,
                   'H_Model_description': self.model_description, " H_pred_time": prediction_time,
                   "H_server_name": "naven_dev", " H_pred_movement": forecast_label}
        if forecast_label in ['Rise', 'Steady', 'Fall']:
            self.Collections[ticker].insert_one(payload)
            print(payload)
