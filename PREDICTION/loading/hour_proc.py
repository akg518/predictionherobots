from PREDICTION.loading.loader import *
from time import time
import numpy as np

def gen_sliding_windows(normal, targets):
    for i in normal:

        obj = normal[i]

        df = obj['output'].copy()
        unit = obj['labels']['time_unit']

        now = time()

        number_of_steps_in_window = 30

        unit = TIME_UNIT_SECONDS[unit]

        seconds_ago = 0
        time_window = unit * number_of_steps_in_window
        b = now - seconds_ago
        a = b - time_window

        (X, Y) = ([], [])
        running = True
        while 1:

            b = now - seconds_ago
            a = b - time_window

            temp_df = df.loc[(a < df.index) & (df.index < b)]

            if len(temp_df) < 3:
                break
            if len(temp_df) == number_of_steps_in_window:
                try:
                    X.append(temp_df)
                    if obj['labels']['poll'] == 'ohlc':
                        tyf = targets[i]['output'].copy()
                        temp_yf = tyf.loc[b < tyf.index].iloc[0]

                        binned_y = pd.cut(temp_yf, [-999999, -0.0005, 0.0005, 999999])  # ['ETH']['ETHohlctarget']
                        dummies = pd.get_dummies(binned_y)

                        Y.append(dummies)
                except:
                    pass

            seconds_ago += unit
        np.save(obj['dir'] + 'X_Y_tuple.npy', (X, Y))


if __name__ == "__main__":
    normal, targets = load_and_preprocess(use_stable_version=False)
    if not SKIP_SLIDING_WINDOWS:
        gen_sliding_windows(normal, targets)
