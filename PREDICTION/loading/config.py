PROJECT_ROOT = './' #'../../'
DATA_DIR = PROJECT_ROOT + 'DATA/Prediction/'

TIME_UNIT_SECONDS = {'hour': 60 * 60, 'minute': 60}

RAW_BLACKLIST = ['volumeto']  # redundant features
GEN_FEATURES_FROM_TIME = 1450000000  # unix seconds
# 1546300800 is unix timestamp for start of 2019


DEBUG_LOADER = False  # causes loader to load only a single dataset
SKIP_PULLING = False
SKIP_SOCIAL = True
SKIP_MINUTE = True
SKIP_AGGREGATING = True
SKIP_SLIDING_WINDOWS = True

GENERATE_SMA_FEATURES = True
GENERATE_EMA_FEATURES = True  # SMAs have better variance
GENERATE_VOLATILITY_FEATURES = True
GENERATE_VAP_FEATURES = True
GENERATE_MINMAX_FEATURES = True  # DO NOT USE - NO VALUE ADDED!

SCALE_FEATURES = True
USE_CURRENT_SCALER = False

USE_TALIB = True

POOL_WORKERS = 4

############ FEATURE GENERATION PARAMETERS ############

SMA_WINDOWS = [2, 5, 10, 24, 48]
EMA_HALFLIFES = [2, 5, 10, 24, 48]
VOLATILITY_WINDOWS = [5, 10, 24, 48]
MINMAX_WINDOWS = [5, 10, 24, 48]

# Volume at Price parameters for searching for support and resistance points
VAP_RANGE = 168
NO_VAP_BUCKETS = 7
VAP_BUCKET_SIZE = 0.005
