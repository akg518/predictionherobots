import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
from sklearn import decomposition

from PREDICTION.loading import xy_loader
from PREDICTION.loading.loader import load_and_preprocess

processed = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/processed.csv'
raw = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/raw.csv'

whitelist = None
blacklist = None

#normal, targets = load_and_preprocess(use_stable_version=False)
X, y = xy_loader.load_XY(raw, processed, return_ndarray=False)

if whitelist is not None:
    X = X[whitelist]

if blacklist is not None:
    new_cols = [col for col in X.columns if col not in blacklist]
    X = X[new_cols]

pca = decomposition.KernelPCA(kernel='rbf', gamma=10)
X_new = pd.DataFrame(pca.fit_transform(X))
X_new.columns = ['PC{}'.format(i) for i in range(X_new.shape[1])]
X_new.index = X.index

feature_variance = X.describe().loc['std'] ** 2
pca_variance = X_new.describe().loc['std'] ** 2

print('total feature variance: {}'.format(sum(feature_variance)))
print('total component variance: {}'.format(sum(pca_variance)))

print('PCA statistics')
stats = X_new.describe().T[['mean', 'std']]
stats['var'] = pca_variance
stats['var %'] = (pca_variance / sum(pca_variance)) * 100
print(stats)

fig = plt.figure(1, figsize=(10, 10))
ax = Axes3D(fig)
X_rise = X_new[y == 'Rise'].iloc[-200:]
X_fall = X_new[y == 'Fall'].iloc[-200:]
X_steady = X_new[y == 'Steady'].iloc[-200:]
ax.scatter(X_rise['PC0'], X_rise['PC1'], X_rise['PC2'], label='Rise')
ax.scatter(X_fall['PC0'], X_fall['PC1'], X_fall['PC2'], label='Fall')
ax.scatter(X_steady['PC0'], X_steady['PC1'], X_steady['PC2'], label='Steady')
plt.legend()
plt.show()
