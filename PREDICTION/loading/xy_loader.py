import pandas as pd
import numpy as np

processed = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/processed.csv'
raw = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/raw.csv'


def load_XY(raw_file, processed_files, return_ndarray=True):
    df = {}
    for df_name, file_name in [('processed', processed_files), ('raw', raw_file)]:
        if df_name == 'processed' and type(processed_files) is list:
            df['processed'] = pd.DataFrame()
            for file in processed_files:
                df_temp = pd.read_csv(file, index_col='time')
                df_temp.index = pd.to_datetime(df_temp.index, unit='s')
                if df['processed'].empty:
                    df['processed'] = df_temp
                else:
                    df['processed'] = pd.merge(df['processed'], df_temp, how='outer', left_index=True, right_index=True)
        else:
            df[df_name] = pd.read_csv(file_name, index_col='time').dropna()
            df[df_name].index = pd.to_datetime(df[df_name].index, unit='s')

        # only keep 2018+ values
        df[df_name] = df[df_name].dropna().loc['2018-01-01':]

    df['raw'] = df['raw'][~df['raw'].index.duplicated(keep='first')]  # remove duplicates and live values

    t1 = df['raw']['close']
    t2 = df['raw']['close'].shift(-2)
    y = (t2 - t1) / t1

    # convert Y into buckets
    y_buckets = pd.cut(y, [-99999, -0.0005, 0.0005, 99999], labels=['Fall', 'Steady', 'Rise'])

    if return_ndarray:
        return df['processed'].iloc[:-2].values, y_buckets[:-2].astype('str').values
    else:
        return df['processed'].iloc[:-2], y_buckets[:-2].astype('str')

def load_features_at(features_file, utctime):
    df = pd.read_csv(features_file, index_col='time')
    df.index = pd.to_datetime(df.index, unit='s', utc=True)
    features = df[utctime:].iloc[0:1].values
    return features


def harry_load_XY(processed_targets, processed_file):

    df = {}
    for df_name, file_name in [('processed', processed_file), ('targets', processed_targets)]:
        df[df_name] = pd.read_csv(file_name,index_col='time')#.dropna() #, index_col='time'
        df[df_name].index = pd.to_datetime(df[df_name].index, unit='s')

        # only keep 2018+ values
        df[df_name] = df[df_name].loc['2018-01-01':]


    # convert Y into buckets
    y=df['targets']['close']
    buckets = pd.IntervalIndex.from_tuples(data=[(-99999, -0.0005), (-0.0005, 0.0005), (0.0005, 99999)])

    y_buckets = pd.cut(y, buckets, labels=['down', 'steady', 'up'])

    return df['processed'].iloc[:-2].values, y_buckets[:-2].astype('str').values

def gen_function_XY(func, no_samples, no_features, clip_zeros=True, noise=0.01):
    """generates normal features and calculates function with noise"""
    eps = 0.001  # values which are too small to zero
    X = np.random.normal(0, 1, (no_samples, no_features))
    if clip_zeros:
        X[np.abs(X) < eps] = eps * np.sign(X[np.abs(X) < eps])

    # apply function and add noise
    y = np.apply_along_axis(func, 1, X).reshape((no_samples, 1))
    y += np.random.normal(0, noise, (no_samples, 1))
    return X, y