import logging
import pickle
from datetime import timedelta

from sklearn.svm import SVC
from slacker_log_handler import SlackerLogHandler, NoStacktraceFormatter

from PREDICTION.loading import loader, config, xy_loader
from PREDICTION.prediction.predictor_base import BasePredictor

DEFAULT_LABEL = 'Rise'

BTC_FEATURES_FILEPATH = 'DATA/Prediction/BTC/ohlc/USD/CCCAGG/hour/processed.csv'
ETH_FEATURES_FILEPATH = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/processed.csv'
BTC_MODEL_FILEPATH = 'DATA/Prediction/MODELS/BTC_SVC.pickle'
ETH_MODEL_FILEPATH = 'DATA/Prediction/MODELS/ETH_SVC.pickle'


class SVCHourPredictor(BasePredictor):
    def __init__(self):
        self.slack_handler = SlackerLogHandler(
            'xoxp-537340929715-537340930979-538023193397-5180882d39a7f7b45d8d5e73881bf994',
            '#log', stack_trace=True, icon_emoji=':robot_face:', ping_level=logging.error)
        self.logger = logging.Logger('PredictionHerobots', level=logging.DEBUG)
        self.logger.addHandler(self.slack_handler)
        self.formatter = NoStacktraceFormatter('%(asctime)s - %(filename)s - %(levelname)s:%(message)s')
        self.slack_handler.setFormatter(self.formatter)
        self.slack_handler.setLevel(logging.DEBUG)

    def is_config_ok(self):
        if config.SKIP_PULLING:
            self.logger.error('config file improperly configured! Set SKIP_PULLNG to false')
            return False
        return True

    def _predict(self, model_filepath, features_filepath, utc_time):
        if not self.is_config_ok():
            return DEFAULT_LABEL
        loader.load_and_preprocess(use_stable_version=False)
        with open(model_filepath, 'rb') as f:
            model: SVC = pickle.load(f)

        corrected_time = utc_time.replace(minute=0, second=0, microsecond=0) - timedelta(hours=1)
        self.logger.info("SVC predicting on the time bucket of {} ".format(corrected_time))
        features = xy_loader.load_features_at(features_filepath, corrected_time)
        return model.predict(features)[0]

    def predict_ETH(self, utc_time):
        return self._predict(ETH_MODEL_FILEPATH, ETH_FEATURES_FILEPATH, utc_time)

    def predict_BTC(self, utc_time):
        return self._predict(BTC_MODEL_FILEPATH, BTC_FEATURES_FILEPATH, utc_time)