"""
============================================================
A simple dense nn
============================================================


"""

from __future__ import print_function

from sklearn.model_selection import train_test_split
from PREDICTION.loading.xy_loader import harry_load_XY
import numpy as np

print(__doc__)



from keras.layers import Input, Dense
from keras.models import Model
from keras import optimizers
import pandas as pd

def Dense_NN(X, y):
    # Split the dataset in two parts
    #y=pd.get_dummies(y)
    mask = [a=='nan'for a in y]^~np.isnan(X).any(axis=1)
    X,y=X[mask],y[mask]
    print(X,y)
    y=pd.get_dummies(y)
    global X_test,y_test
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, shuffle=False)
    print(X_train.shape)
    # This returns a tensor
    inputs = Input(shape=(X_train.shape[1],))

    # a layer instance is callable on a tensor, and returns a tensor
    x = Dense(512, activation='relu')(inputs)
    #data=BatchNormalization()(data)
    x = Dense(1024, activation='relu')(x)
    #data = BatchNormalization()(data)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    #data = BatchNormalization()(data)
    predictions = Dense(3, activation='softmax')(x)

    # This creates a model that includes
    # the Input layer and three Dense layers
    adam = optimizers.Adam()

    model = Model(inputs=inputs, outputs=predictions)
    model.compile(optimizer=adam,
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    #print(pd.get_dummies(y_train))
    model.fit(X_train, y_train,validation_split=0.2,epochs=50,batch_size=128)

    print("test_accuracy: "+str((model.predict(X_test).argmax(axis=1)==np.array(y_test).argmax(axis=1)).sum()/len(X_test)))
    #model.predict(X_test)-pd.get_dummies(y_test)
    return model



processed = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/minute/processed.csv'
raw = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/raw.csv'
targets='DATA/Prediction/ETH/ohlc/USD/CCCAGG/minute/targets_processed.csv'

trained=Dense_NN(*harry_load_XY(targets, processed))#*load_XY(raw, processed)

#trained.predict(X_test).argmax(axis=1)

# pd.read_csv(processed),pd.read_csv('../../DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/targets_processed.csv')
# Note the problem is too easy: the hyperparameter plateau is too flat and the
# output model is the same for precision and recall with ties in quality.
# use this kfold instead https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.TimeSeriesSplit.html
