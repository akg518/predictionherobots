"""
============================================================
Parameter estimation using bayesian optimisation hyperparameter optimisation
============================================================
"""
from __future__ import print_function

import numpy as np
import os

from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelEncoder
from skopt import BayesSearchCV

REL_PATH_TO_WDIR = "../../"
if 'PREDICTION' not in os.listdir('.'):
    print("CHANGING WORKING DIRECTORY...")
    print("current wd: {}".format(os.getcwd()))
    os.chdir(REL_PATH_TO_WDIR)
    print("new wd: {}".format(os.getcwd()))

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVR, SVC, LinearSVC
from PREDICTION.loading.xy_loader import load_XY
from PREDICTION.prediction.util import oversample_label
import pickle
from PREDICTION.loading.loader import load_and_preprocess
from imblearn.over_sampling import SMOTE

print(__doc__)

def grid_search(X, y, resample=False, svm_type="SVC"):
    # Split the dataset in two parts
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2)

    no_features = X.shape[1]

    if resample:
        # oversample/undersample the underrepresented state
        sampler = SMOTE()
        X_train, y_train = sampler.fit_resample(X_train, y_train)

    # Set the parameters by cross-validation
    param_space = {
        'C': (4, 8, 'log-uniform'),
        'gamma': (0.01, 0.05, 'log-uniform'),
        'kernel': ['rbf'],  # categorical parameter
    }

    print("# Tuning hyper-parameters for accuracy")
    print()

    if svm_type == 'SVR':
        predictor = SVR()
        scoring = 'neg_mean_squared_error'
    elif svm_type == 'SVC':
        predictor = SVC()
        scoring = 'accuracy'
    elif svm_type == 'LinearSVC':
        predictor = LinearSVC(max_iter = 5000)
        scoring = 'accuracy'
    else:
        raise ValueError("Incorrect type provided!")
    clf = BayesSearchCV(predictor, param_space, cv=4, verbose=5, n_jobs=-1, scoring=scoring)
    clf.fit(X_train, y_train)

    print("Best parameters set found on development set:")
    print()
    print(clf.best_params_)
    print()
    print("Grid scores on development set:")
    print()
    means = clf.cv_results_['mean_test_score']
    stds = clf.cv_results_['std_test_score']
    for mean, std, params in zip(means, stds, clf.cv_results_['params']):
        print("%0.3f (+/-%0.03f) for %r"
              % (mean, std * 2, params))
    print()

    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()
    if not svm_type:
        y_true, y_pred = y_test, clf.predict(X_test)
        print(classification_report(y_true, y_pred))
    print()
    return clf


processed = ['DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/processed.csv',
             'DATA/Prediction/BTC/ohlc/USD/CCCAGG/hour/processed.csv']
raw = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/raw.csv'
targets = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/targets_processed.csv'

clf = grid_search(*load_XY(raw, processed), resample=False)
