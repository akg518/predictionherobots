"""
common prediction utilities
"""
import pandas as pd
import random
from sklearn.utils import shuffle

def oversample_label(X, y, label, oversampling_ratio=None, seed=None):
    """
    Performs simple oversampling by duplicating the records which are to be oversampled.
    :param X: X dataset, dataframe
    :type X: pd.DataFrame
    :param y: y dataset, series
    :type y: pd.Series
    :param label: which label is to be oversampled
    :param oversampling_ratio: set how much data should be duplicated, if set to None label will be averaged
    :param seed: the random seed, used for debug
    :return: oversampled tuple of X, y, both nparrays
    """
    # converting to correct types
    if type(X) is not pd.DataFrame:
        X = pd.DataFrame(X)
    if type(y) is not pd.Series:
        y = pd.Series(y)

    if oversampling_ratio is None:
        # manually set the oversampling ratio
        values = y.value_counts()
        target_label_size = len(y)/(len(values)-1)
        oversampling_ratio = target_label_size/values[label]

    if oversampling_ratio <= 1:
        return X, y

    if seed is not None:
        random.seed(seed)

    label_indices = y[y == label]
    label_rownum = len(label_indices)
    target_rownum = int(label_rownum * oversampling_ratio)

    for i in range(target_rownum-label_rownum):
        sample_ix = label_indices.index[random.randint(0, label_rownum-1)]
        X = X.append(X.loc[sample_ix], ignore_index=True)
        y = y.append(y.loc[sample_ix:sample_ix], ignore_index=True)

    return shuffle(X.values, y.values, random_state=seed)
