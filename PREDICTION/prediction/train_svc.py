from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from PREDICTION.loading.xy_loader import load_XY
from PREDICTION.prediction import util
import pickle
import os
import pandas as pd


def save_model(raw_file, processed_file, data_folder, model_filename):
    X, y = load_XY(raw_file, processed_file)
    X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.05, shuffle=False)

    #print("oversampling steady label")
    #X_train, y_train = util.oversample_label(X_train, y_train, 'Steady', 2)

    print("training model...")
    linear_model = SVC(C=5, kernel='rbf', gamma=0.04, verbose=True, probability=True)
    linear_model.fit(X_train, y_train)
    print("model trained!")

    print("classification record:")
    print("TRAINING METRICS...")
    print(classification_report(y_train, linear_model.predict(X_train)))
    print()
    print("TESTING METRICS...")
    print(classification_report(y_test, linear_model.predict(X_test)))

    print("saving model...")
    if 'MODELS' not in os.listdir(data_folder):
        os.mkdir(os.path.join(data_folder, 'MODELS'))
    with open(os.path.join(data_folder, 'MODELS', model_filename), 'wb') as f:
        pickle.dump(linear_model, f)

    print("model saving complete!")


def load_model(model_filepath):
    with open(model_filepath, 'rb') as f:
        return pickle.load(f)


if __name__ == "__main__":
    token_used = 'ETH'
    processed_template ='DATA/Prediction/{}/ohlc/USD/CCCAGG/hour/processed.csv'
    processed_files = [processed_template.format(token) for token in ['ETH', 'BTC']]
    raw_file = 'DATA/Prediction/{}/ohlc/USD/CCCAGG/hour/raw.csv'.format(token_used)
    data_folder = 'DATA/Prediction'
    model_filename = '{}_SVC.pickle'.format(token_used)
    model_filepath = os.path.join(data_folder, 'MODELS', model_filename)

    save_model(raw_file, processed_files, data_folder, model_filename)
    model = load_model(model_filepath)

    # loading for comparison
    X, y = load_XY(raw_file, processed_files)
    results = pd.DataFrame()
    results['y'] = y
    results['y_hat'] = model.predict(X)
    results['same'] = results['y'] == results['y_hat']

