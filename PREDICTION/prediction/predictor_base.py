"""defines specifications for predictor class"""
from abc import ABC, abstractmethod


class BasePredictor(ABC):
    @abstractmethod
    def predict_ETH(self, utc_time):
        """
        predict the value of ETC
        :param utc_time: Time for which to make the prediction. Formatted as UTC.
        :return: Rise/Steady/Fall
        """
        pass

    @abstractmethod
    def predict_BTC(self, utc_time):
        """
        predict the value of ETC
        :param utc_time: Time for which to make the prediction. Formatted as UTC.
        :return: Rise/Steady/Fall
        """
        pass
