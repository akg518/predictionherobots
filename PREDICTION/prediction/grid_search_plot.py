import pickle

import matplotlib.pyplot as plt
import numpy as np


def plot_grid(filename=None, clf=None, x_param='param_gamma', y_param='param_C'):
    GRID_SEARCH_FILEPATH = 'grid_search.pkl'

    if filename is None:
        filename = GRID_SEARCH_FILEPATH
    if clf is None:
        with open(filename, 'rb') as f:
            clf = pickle.load(f)

    cv_results = clf.cv_results_

    x_axis = sorted(list(set(cv_results[x_param])))
    y_axis = sorted(list(set(cv_results[y_param])))

    mean_pixels = np.reshape(cv_results['mean_test_score'], (len(y_axis), len(x_axis)))
    std_pixels = np.reshape(cv_results['mean_train_score'], (len(y_axis), len(x_axis)))

    fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(20, 10))

    im = ax1.imshow(mean_pixels, cmap='nipy_spectral', interpolation='gaussian', extent=[-8, -3, 7, 1], origin='upper')
    ax1.set_title('Mean Test Score')
    fig.colorbar(im, ax=ax1)
    ax1.set_xlabel(x_param)
    ax1.set_ylabel(y_param)

    im = ax2.imshow(std_pixels, cmap='nipy_spectral', interpolation='gaussian', extent=[-8, -3, 7, 1], origin='upper')
    ax2.set_title('Mean Train Score')
    fig.colorbar(im, ax=ax2)
    ax2.set_xlabel(x_param)
    ax2.set_ylabel(y_param)

    plt.show()

if __name__ == "__main__":
    plot_grid()