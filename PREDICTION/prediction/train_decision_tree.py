import pickle

import numpy as np
import pandas as pd
from sklearn import decomposition
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from PREDICTION.prediction import grid_search_plot
from xgboost import XGBClassifier
from skopt import BayesSearchCV

from PREDICTION.loading import xy_loader, loader

processed = ['DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/processed.csv',
             'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/patterns.csv']
raw = 'DATA/Prediction/ETH/ohlc/USD/CCCAGG/hour/raw.csv'

X, y = xy_loader.load_XY(raw, processed, return_ndarray=False)

pca = decomposition.PCA()
X_new = pd.DataFrame(pca.fit_transform(X.values))

feature_names = X.columns

X_train, X_test, y_train, y_test = train_test_split(
    X_new, y.values, test_size=0.2, shuffle=False)

params = {
    'max_depth': (10, 100),
    'max_features': (1, 100),
    'min_samples_leaf': (1, 100),
    'min_samples_split': (1, 100),
    'n_estimators': (20, 10000)
}

predictor = RandomForestClassifier()

result = pd.DataFrame()
result['y'] = y_test

print("Training the parameters...")
clf = BayesSearchCV(predictor, params, cv=4, verbose=5, n_jobs=4)
clf.fit(X_train, y_train)

print("Best parameters set found on development set:")
print()
print(clf.best_params_)
print()
print("Grid scores on development set:")
print()
means = clf.cv_results_['mean_test_score']
stds = clf.cv_results_['std_test_score']
for mean, std, params in zip(means, stds, clf.cv_results_['params']):
    print("%0.3f (+/-%0.03f) for %r"
          % (mean, std * 2, params))
print()

with open('grid_search_gdboost.pkl', 'wb') as f:
    pickle.dump(clf, f)

feature_importance = pd.Series(clf.best_estimator_.feature_importances_).sort_values()
feature_importance.index = feature_names
feature_importance[feature_importance > 0.01].plot.barh(
    title="technicals feature importance (accuracy: {})".format(clf.best_score_))
#
# grid_search_plot.plot_grid(clf=clf, x_param='param_max_features', y_param='param_max_depth')