"""
this dummy predictor outputs Rise all the times and is used for testing
"""
from PREDICTION.prediction.predictor_base import BasePredictor


class DummyPredictor(BasePredictor):
    def predict_ETH(self, utc_time):
        return 'Rise'

    def predict_BTC(self, utc_time):
        return 'Rise'

