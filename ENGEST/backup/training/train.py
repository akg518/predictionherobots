import pandas as pd
from keras.callbacks import ModelCheckpoint
from keras.layers import *
from keras import *
from ENGEST.config import *


def gen_xy(x_filepath, y_filepath, validate=False):
    """
    batch generator, generates batches of data data and y labels for preprocessing
    :param x_filepath: filepath to encoder tokens
    :param y_filepath: filepath to decoder tokens
    :param validate: if validate is true it will use small subset of the training set to validate
    :return: [x_enc_tokens, x_dec_tokens], y_onehot_vectors
    """
    X_full: np.ndarray = pd.read_parquet(x_filepath).values
    Y_full: np.ndarray = pd.read_parquet(y_filepath).values
    no_rows = X_full.shape[0]
    y_len = Y_full.shape[1]
    start_ix = int(no_rows * VALIDATE_OFFSET)
    end_ix = int(no_rows * (VALIDATE_OFFSET + VALIDATE_SET))
    if validate:
        # get the validation set
        X = X_full[start_ix:end_ix]
        Y = Y_full[start_ix:end_ix]
        no_rows = X.shape[0]
    else:
        # get the training set
        X = np.concatenate((X_full[:start_ix], X_full[end_ix:]))
        Y = np.concatenate((Y_full[:start_ix], Y_full[end_ix:]))
        no_rows = X.shape[0]
    current_ix = 0
    while True:
        x_enc = X[current_ix:current_ix + BATCH_SIZE]
        x_dec = Y[current_ix:current_ix + BATCH_SIZE]
        y = np.zeros((BATCH_SIZE, y_len, NO_TOKENS))
        token_rows, token_cols = np.where(x_dec != 0)
        token_dims = x_dec[token_rows, token_cols]
        y[token_rows, token_cols, token_dims] = 1
        if validate:
            current_ix = int((current_ix + BATCH_SIZE) % (no_rows - (no_rows * VALIDATE_OFFSET)))
        else:
            current_ix = int((current_ix + BATCH_SIZE) % (no_rows - BATCH_SIZE))
        yield [x_enc, x_dec[:, :-1]], y[:, 1:, :]


def train_model(x_filepath, y_filepath):
    # Define an input sequence and process it.
    encoder_inputs = Input(shape=(None,))
    x = Embedding(NO_TOKENS, LATENT_DIM)(encoder_inputs)
    #data= LSTM(LATENT_DIM,return_sequences=True)(data)
    x, state_h, state_c = LSTM(LATENT_DIM, return_state=True)(x)
    #
    encoder_states = [state_h, state_c]

    # Set up the decoder, using `encoder_states` as initial state.
    decoder_inputs = Input(shape=(None,))
    x = Embedding(NO_TOKENS, LATENT_DIM)(decoder_inputs)
    x, _, _ = LSTM(LATENT_DIM, return_sequences=True, return_state=True)(x, initial_state=encoder_states)
    #data = LSTM(LATENT_DIM,return_sequences=True)(data)
    decoder_outputs = Dense(NO_TOKENS, activation='softmax')(x)

    # Define the model that will turn
    # `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
    model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

    # Compile & run training
    sgd = optimizers.rmsprop(clipvalue=0.5)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])
    checkpoint = ModelCheckpoint(TEMP_MODEL_WEIGHTS_FILEPATH, monitor='val_acc', verbose=1, save_best_only=True,
                                 mode='max')
    callbacks_list = [checkpoint]
    model.summary()
    model.fit_generator(gen_xy(x_filepath, y_filepath), STEPS_PER_EPOCH, EPOCH_NO,
                        validation_data=gen_xy(x_filepath, y_filepath, True), validation_steps=20, verbose=1,
                        callbacks=callbacks_list)
    model.save(FINAL_MODEL_FILEPATH)
