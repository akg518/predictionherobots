from ENGEST.preprocessing.utils import *
from keras.models import *
from keras.layers import *
import pandas as pd
from ENGEST.config import *

model = load_model(FINAL_MODEL_FILEPATH)

encoder_inputs = model.input[0]  # input_1
encoder_outputs, state_h_enc, state_c_enc = model.layers[4].output  # lstm_1
encoder_states = [state_h_enc, state_c_enc]
encoder_model = Model(encoder_inputs, encoder_states)

decoder_inputs = model.input[1]  # input_2
decoder_state_input_h = Input(shape=(LATENT_DIM,), name='input_3')
decoder_state_input_c = Input(shape=(LATENT_DIM,), name='input_4')
decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
decoder_embedding = model.layers[3]  # decoder_embedding
x = decoder_embedding(decoder_inputs)
# data = Embedding(NO_TOKENS, LATENT_DIM, weights=decoder_embedding.get_weights()[0], trainable=False)(decoder_inputs)
decoder_lstm = model.layers[5]  # lstm_2
decoder_lstm.return_state = True
decoder_outputs, state_h_dec, state_c_dec = decoder_lstm(x, initial_state=decoder_states_inputs)
decoder_states = [state_h_dec, state_c_dec]
decoder_dense = model.layers[6]
decoder_outputs = decoder_dense(decoder_outputs)
decoder_model = Model([decoder_inputs] + decoder_states_inputs, [decoder_outputs] + decoder_states)


df_en_word2token = pd.read_csv(DICT_FILE_EN, index_col=0)
df_et_token2word = pd.read_csv(DICT_FILE_ET, index_col='hash')
translate_df = pd.read_csv(MORPHEME_TRANSLATION_TABLE_EN, index_col='word')
MAX_TRANSLATE_LENGTH = 40

def get_tokens(sentence, dict_file):
    words = preprep_line(sentence, translate_df)
    tokens = [dict_file.at[word, 'hash'] if word in dict_file.index else WORD_CAP+1 for word in words]
    current_unknown_word = 3
    for ix in range(len(tokens)):
        if tokens[ix] > WORD_CAP:
            tokens[ix] = current_unknown_word
            current_unknown_word = min(current_unknown_word+1, 7)
        else:
            tokens[ix] += EXTRA_TOKENS
    return tokens

def get_sentence(tokens, dict_file):
    new_tokens = []
    for token in tokens:
        if token == 5:
            break
        new_tokens.append(token - EXTRA_TOKENS)
    # TODO put morphemes back into individual words
    return ' '.join([dict_file.at[token, 'Unnamed: 0'] for token in new_tokens if token>EXTRA_TOKENS])

def decode_sequence(input_seq):
    # use the model to get a list of tokens
    tokens = np.array(get_tokens(input_seq, df_en_word2token))

    # generate token prediction
    states_value = encoder_model.predict(tokens)
    sampled_token_index = 1
    translated_tokens = []
    while True:
        output_tokens, h, c = decoder_model.predict([np.array([[sampled_token_index]])] + states_value)

        # Sample a token
        sampled_token_index = np.argmax(output_tokens[0, -1, 9:])
        sampled_token_max_p = np.max(output_tokens[0, -1, 9:])
        end_token_p = output_tokens[0, -1, 5]

        if end_token_p > sampled_token_max_p or len(translated_tokens) > MAX_TRANSLATE_LENGTH:
            break
        translated_tokens.append(output_tokens[0][0][sampled_token_index])
        # Update states
        states_value = [h, c]

    # decode the tokens back to a string
    sentence = get_sentence(translated_tokens, df_et_token2word)

    return sentence