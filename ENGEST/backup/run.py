import os
import sys
import datetime

# CHANGING THE WORKING DIRECTORY
if len(sys.argv) != 2:
    print("please insert the working directory in the script parameter!")
    exit(1)

WORKING_DIR = sys.argv[1]
sys.path.append(WORKING_DIR)

os.chdir(WORKING_DIR)

# WORKING DIRECTORY CONDITIONAL IMPORTS
from ENGEST.preprocessing import run_stage, gen_morphemes
from ENGEST.config import *
from ENGEST.training import train


RAW_FILES = ['europarl-v7.et-en.en', 'europarl-v7.et-en.et']


def run_complete_preprocessing_pipeline():
    response = input("this is going to reset your data, are you sure? Y/n")
    if response.lower() == 'y':
        print("BACKING UP OLD FILES...")
        if not os.path.isdir(BACKUP_DIR):
            os.makedirs(BACKUP_DIR)
        datetime_str = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        os.makedirs(os.path.join(BACKUP_DIR, datetime_str))
        move_files = [f for f in os.listdir(DATA_DIR)
                      if os.path.isfile(os.path.join(DATA_DIR, f)) and f not in RAW_FILES]
        for move_file in move_files:
            os.rename(os.path.join(DATA_DIR, move_file), os.path.join(BACKUP_DIR, datetime_str, move_file))

        print("GENERATING VOCABULARIES (STAGES 1-5)...")
        gen_vocabulary_stages = [1, 2, 3, 4, 5]
        for stage in gen_vocabulary_stages:
            run_stage(stage, use_morpheme_translation_table=False)

        print("CREATING MORPHEME TRANSLATION TABLES")
        print("ENGLISH FILE...")
        gen_morphemes.generate_morphemes(DICT_FILE_EN, MORPHEME_TRANSLATION_TABLE_EN)
        print("ESTONIAN FILE...")
        gen_morphemes.generate_morphemes(DICT_FILE_ET, MORPHEME_TRANSLATION_TABLE_ET)

        print("BACKING UP OLD VOCABULARY FILES...")
        os.rename(DICT_FILE_EN, os.path.join(DATA_DIR, 'en_dict_old.csv'))
        os.rename(DICT_FILE_ET, os.path.join(DATA_DIR, 'et_dict_old.csv'))

        print("RUNNING FULL PIPELINE WITH MORPHEME TRANSLATION (STAGES 1-6)...")
        pipeline_with_morpheme_translation_steps = [1, 2, 3, 4, 5, 6]
        for stage in pipeline_with_morpheme_translation_steps:
            run_stage(stage, use_morpheme_translation_table=True)

        print("PREPROCESSING COMPLETE!")


def run_model_training():
    if not all(map(os.path.isfile, [PARQUET_FILE_EN.format(6), PARQUET_FILE_ET.format(6)])):
        print("preprocessed files missing! Please run all of the preprocessing stages first!")
        exit(1)
    print("TRAINING THE MODEL...")
    train.train_model(PARQUET_FILE_EN.format(6), PARQUET_FILE_ET.format(6))
    print("TRAINING COMPLETE!")


def decode(input_sentence):
    if not os.path.isfile(FINAL_MODEL_FILEPATH):
        print("trained model not found! Please train the model first!")
        exit(1)
    from ENGEST.training import decode
    return decode.decode_sequence(input_sentence)

MANUAL_RUN_STAGES = []  # insert numbers corresponding to stages that you want to run
for stage in sorted(MANUAL_RUN_STAGES):
    run_stage(stage)
