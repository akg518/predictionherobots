import random, sys
random.seed(38710)

WORKING_DIR = sys.argv[1]

DATA_DIR = WORKING_DIR + 'DATA/ENGEST/'
BACKUP_DIR = DATA_DIR + 'BACKUP/'

RAW_FILE_EN = DATA_DIR + "europarl-v7.et-en.en"
RAW_FILE_ET = DATA_DIR + "europarl-v7.et-en.et"
VARS_FILE = DATA_DIR + "vars.json"
PARQUET_FILE_EN = DATA_DIR + "en_STAGE{}.parquet"
PARQUET_FILE_ET = DATA_DIR + "et_STAGE{}.parquet"
DICT_FILE_EN = DATA_DIR + "en_dict.csv"
DICT_FILE_ET = DATA_DIR + "et_dict.csv"
HASH_MAPPING = DATA_DIR + "hash_mapping.csv"
MORPHEME_TRANSLATION_TABLE_EN = DATA_DIR + "morpheme_translate_en.csv"
MORPHEME_TRANSLATION_TABLE_ET = DATA_DIR + "morpheme_translate_et.csv"
TEMP_MODEL_WEIGHTS_FILEPATH = DATA_DIR + "weights.best.hdf5"
FINAL_MODEL_FILEPATH = DATA_DIR + "model_final.hdf5"

# used for preprocessing files
KEEP_PERCENTAGE_LENGTH = "95%"  # this is for dropping some very long sequences

# used for multiprocessing
STAGE_1_CHUNK_SIZE = 10000
STAGE_2_CHUNK_SIZE = 50000
STAGE_6_CHUNK_SIZE = 10000
THREAD_POOL_SIZE = 4

# used for generating a morpheme translation table
MIN_ROOT_LENGTH = 3
POSTFIX_KEEP_NUMBER = 50

# this is used to convert individual words into a sequence of morphemes (only at preprocessing stages)
USE_MORPHEME_TRANSLATION_TABLE = True

# used for capping the vocabulary (stage 6)
NO_UNKNOWN = 5  # number of unknown english tokens
WORD_CAP = 5000  # max number of tokens
EXTRA_TOKENS = NO_UNKNOWN + 2 + 1  # 2 for start/end, 1 for unknown translate token

SAMPLE_SIZE = -1  # used for testing on the smaller corpus, use -1 if parsing full corpus

# TRAINING VARIABLES
VALIDATE_SET = 0.01
VALIDATE_OFFSET = random.randint(0, 95 - (100 * VALIDATE_SET)) / 100
LATENT_DIM = 256
EPOCH_NO = 15
STEPS_PER_EPOCH = 100
BATCH_SIZE = 32
NO_TOKENS = WORD_CAP + EXTRA_TOKENS + 1
