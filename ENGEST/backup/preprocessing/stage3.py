"""tokenize each word and generate word frequencies, keep N most popular words"""

from .utils import *
from ENGEST.config import *
from collections import Counter
import multiprocessing as mp


def calc_word_freq_chunk(chunk: pd.DataFrame):
    print("starting process {}...".format(mp.current_process()))
    counted = Counter()
    index = chunk.index
    columns = chunk.columns
    no_rows = len(index)
    status_update_number = 10  # how many times to update the status of the process
    status_update_rows = [no_rows // status_update_number * i + index[0] for i in range(status_update_number)]
    status_ix = 0
    status_update_row = status_update_rows[status_ix]
    for row_ix in index:
        if row_ix == status_update_row:
            print("{} - {:.2f}% complete...".format(mp.current_process(), 100 / status_update_number * status_ix))
            status_ix += 1
            if status_ix < status_update_number:
                status_update_row = status_update_rows[status_ix]
        for col_ix in columns[1:]:
            counted[chunk.at[row_ix, col_ix]] += 1
    print("process {} finished!".format(mp.current_process()))
    return counted


def calc_word_freq(in_parquet, dict_file):
    print("deleting old files")
    if os.path.isfile(dict_file):
        os.remove(dict_file)

    print("loading parquet file in chunks")
    chunks = load_parquet_in_chunks(in_parquet, STAGE_2_CHUNK_SIZE, SAMPLE_SIZE)

    print("calculating word frequencies...")
    print("file has been split into {} chunks".format(len(chunks)))
    print("processing file in thread pool with {} workers".format(THREAD_POOL_SIZE))

    with mp.Pool(THREAD_POOL_SIZE) as p:
        counted = sum(p.map(calc_word_freq_chunk, chunks), Counter())
    print("processing complete!")
    print("saving dictionary to csv file")

    index, freq = zip(*counted.most_common())
    en_dict = pd.DataFrame({'hash': range(len(counted)), 'freq': freq}, index=index)
    en_dict.to_csv(dict_file)


def tokenize_chunk(chunk_df, word_hashes):
    index = chunk_df.index
    columns = chunk_df.columns[1:]  # drop line_no

    no_rows = len(index)
    status_update_number = 20  # how many times to update the status of the process
    status_update_rows = [no_rows // status_update_number * i + index[0] for i in range(status_update_number)]
    status_ix = 0
    status_update_row = status_update_rows[status_ix]

    first_line = chunk_df['line_no'].iloc[0]
    last_line = chunk_df['line_no'].iloc[-1]

    print('tokenizing lines {}/{}'.format(first_line, last_line))
    for row_ix in index:
        if row_ix == status_update_row:
            print("{}/{} - {:.2f}% complete...".format(chunk_df.at[row_ix, 'line_no'], last_line,
                                                       100 / status_update_number * status_ix))
            status_ix += 1
            if status_ix < status_update_number:
                status_update_row = status_update_rows[status_ix]
        for col_ix in columns:
            value = chunk_df.at[row_ix, col_ix]
            if value is None:
                chunk_df.at[row_ix, col_ix] = 0
            else:
                chunk_df.at[row_ix, col_ix] = word_hashes[value]
    print('{}/{} - tokenizing complete!'.format(first_line, last_line))
    return chunk_df


def tokenize_file(in_parquet, out_parquet, dict_file):
    """
    take a parquet file from stage 1, tokenize it and save it
    :param in_parquet: the filepath to the stage 1 parquet file
    :param out_parquet: the output file to parquet 2 file
    :param dict_file: the dictionary file to be saved (to convert back)
    """
    print("deleting old files")
    if os.path.isfile(out_parquet):
        os.remove(out_parquet)

    print("loading parquet file in chunks")
    chunks = load_parquet_in_chunks(in_parquet, STAGE_2_CHUNK_SIZE, SAMPLE_SIZE)

    print("loading the dict file")
    df_dict = pd.read_csv(dict_file, index_col=0, keep_default_na=False)
    word_hashes = df_dict['hash'][1:]
    print("dict: {}".format(dict))
    print("Tokenizing file")
    print("File has been split into {} chunks".format(len(chunks)))
    print("Processing file in thread pool with {} workers".format(THREAD_POOL_SIZE))
    with mp.Pool(THREAD_POOL_SIZE) as p:
        df = pd.concat(p.starmap(tokenize_chunk, ((chunk, word_hashes) for chunk in chunks)))
    print("Tokenisation complete!")

    print("Saving tokenized file...")
    df = df.astype('uint32')
    df.to_parquet(out_parquet)


def run_stage_3():
    if not os.path.isfile(PARQUET_FILE_EN.format(2)):
        print("STAGE 2 FILE NOT FOUND, QUITTING NOW!")
        exit(1)
    print("STARTING STAGE 3 PRE-PROCESSING...")
    calc_word_freq(PARQUET_FILE_EN.format(2), DICT_FILE_EN)
    tokenize_file(PARQUET_FILE_EN.format(2), PARQUET_FILE_EN.format(3), DICT_FILE_EN)
    print("STAGE 3 COMPLETE!")
