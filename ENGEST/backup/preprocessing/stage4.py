"""read and preprocess the estonian file, filter both languages for common lines"""
from .utils import *
from .stage2 import convert_to_parquet
from ENGEST.config import *


def run_stage_4(use_morpheme_translation=USE_MORPHEME_TRANSLATION_TABLE):
    print("RUNNING STAGE 4")
    print("CHECKING FOR REQUIRED FILES")
    file_vars = get_vars(VARS_FILE, ['et'])
    if not file_vars:
        print("STAGE 0 VARS MISSING, please re-run!")
        exit(1)
    if not os.path.isfile(PARQUET_FILE_EN.format(3)):
        print("NO STAGE 3 PARQUET FILES FOUND!")
        exit(1)

    print("PREPROCESSING ESTONIAN FILE")
    lines = get_lines(RAW_FILE_ET)[:SAMPLE_SIZE]
    convert_to_parquet(lines, file_vars['et'][KEEP_PERCENTAGE_LENGTH], PARQUET_FILE_ET.format(4), use_morpheme_translation,
                       MORPHEME_TRANSLATION_TABLE_ET)
    del lines
    df_et = pd.read_parquet(PARQUET_FILE_ET.format(4))
    df_en = pd.read_parquet(PARQUET_FILE_EN.format(3))
    print("PREPROCESSING COMPLETE!")

    print("FILTERING BOTH DATAFRAMES FOR COMMON LINES...")
    common_lines = set(df_en['line_no']) & set(df_et['line_no'])
    df_et = df_et.loc[df_et['line_no'].isin(common_lines)].reset_index(drop=True)
    df_et.to_parquet(PARQUET_FILE_ET.format(4))
    df_en = df_en.loc[df_en['line_no'].isin(common_lines)].reset_index(drop=True)
    df_en.to_parquet(PARQUET_FILE_EN.format(4))
    print("FILTERING COMPLETE!")

    print("STAGE 4 COMPLETE")
