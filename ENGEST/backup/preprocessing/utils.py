"""
common utility funcions for preprocessing files
"""

import os
import json
import re
import pandas as pd


def get_vars(vars_filepath, vars=None):
    """helper function, checks if all the necessary variables are in the vars file"""
    if not os.path.isfile(vars_filepath):
        print("json file does not exist, quitting now...")
        return False
    if vars is not None:
        with open(vars_filepath) as f:
            try:
                file_vars = json.load(f)
                if type(file_vars) is not dict:
                    raise json.decoder.JSONDecodeError
            except json.decoder.JSONDecodeError:
                print("json file corrupted, quitting now...")
                return False
            missing_vars = set(vars) - set(file_vars.keys())
            if missing_vars:
                print("variables are missing! {}".format(missing_vars))
                return False
            return file_vars


def update_vars(vars_filepath, new_vars):
    # create file if it doesn't exist
    if not os.path.isfile(vars_filepath):
        open(vars_filepath, 'w+').close()
    # read the contents first and save in memory
    with open(vars_filepath, 'r') as f:
        try:
            temp_vars = json.load(f)
            if type(temp_vars) is not dict:
                raise json.decoder.JSONDecodeError
        except json.decoder.JSONDecodeError:
            temp_vars = {}
    # update and save the new variables, overriding the previous content
    temp_vars.update(new_vars)
    with open(vars_filepath, 'w') as f:
        json.dump(temp_vars, f)


def get_lines(filename):
    with open(filename, 'r', encoding="utf8") as f:
        return f.readlines()


def separate_characters(line):
    """
    separating long numbers and punctuation into individual words
    :param line: the line to be converted
    :return: string with spaces added in the right place
    """
    SEP_LETTERS = "1234567890,.;:?!\-&\{\[\(\}\]\)\\\"\'/”"
    return re.sub(r'([{}])'.format(SEP_LETTERS), r' \1 ', line)


def translate_tokens(words, translate_df):
    translated = []
    translate_index = translate_df.index
    for word in words:
        if word in translate_index:
            translated.append(translate_df.at[word, 'root'])
            postfix = translate_df.at[word, 'postfix']
            if not pd.isnull(postfix):
                translated.append(postfix)
        else:
            translated.append(word)
    return translated


def preprep_line(line, translate_df=None):
    """
    line pre-preparation, splits up the words and returns a list of words
    :param line: the line to do preprep on
    :param translate_df: if some words are to be split up then this file provides the translation
    :return: list of strings
    """
    if translate_df is not None:
        return translate_tokens(separate_characters(line).lower().strip().split(), translate_df)
    return separate_characters(line).lower().strip().split()


def load_parquet_in_chunks(in_parquet, chunk_size, sample_size):
    df = pd.read_parquet(in_parquet).iloc[:sample_size, :]
    no_chunks = len(df) // chunk_size
    if len(df) % chunk_size != 0:
        no_chunks += 1
    return [df.iloc[i * chunk_size: min((i + 1) * chunk_size, len(df)), :] for i in range(no_chunks)]
