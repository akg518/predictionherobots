"""tokenize the estonian file"""

from .stage3 import *


def run_stage_5():
    if not os.path.isfile(PARQUET_FILE_ET.format(4)):
        print("STAGE 4 FILE NOT FOUND, QUITTING NOW!")
        exit(1)
    print("STARTING STAGE 5 PRE-PROCESSING...")
    # calc_word_freq(PARQUET_FILE_ET.format(4), DICT_FILE_ET)
    tokenize_file(PARQUET_FILE_ET.format(4), PARQUET_FILE_ET.format(5), DICT_FILE_ET)
    print("STAGE 5 COMPLETE!")
