"""
large estonian dictionary size is attributed to root word differentiation.
To reduce the number of the dictionary automatic morpheme identification algorithm will be created below.
"""

from collections import Counter
import pandas as pd
from ENGEST.config import *


def get_next_letter_point(words, start_word_ix, end_word_ix, letter_ix):
    """performs bst search to find where the letters are switching"""
    start_word, end_word = words[start_word_ix], words[end_word_ix]
    lower_letter, upper_letter = start_word[letter_ix], end_word[letter_ix]
    if lower_letter == upper_letter:
        return False
    lower_ix, upper_ix = start_word_ix, end_word_ix
    while lower_ix != upper_ix - 1:
        current_ix = (lower_ix + upper_ix) // 2
        current_letter = words[current_ix][letter_ix]
        if current_letter == lower_letter:
            lower_ix = current_ix
        else:
            upper_ix = current_ix
    return upper_ix


def get_root_postfix_scores(words, start_word_ix, end_word_ix, letter_ix):
    """divide and conquer algorithm to split all root blocks and rank them accordingly
    :returns: counter(root_scores), counter(postfix_scores)"""
    if start_word_ix == end_word_ix:  # base case
        return Counter(), Counter()

    root_scores = Counter()
    postfix_scores = Counter()

    if letter_ix > MIN_ROOT_LENGTH:
        score = letter_ix * (end_word_ix - start_word_ix + 1)
        root_scores[words[start_word_ix][:letter_ix]] = score
        for word_ix in range(start_word_ix, end_word_ix + 1):
            if letter_ix < len(words[word_ix]) - 1:
                postfix_scores[words[word_ix][letter_ix + 1:]] += score

    # adjustment if the letter checked the end of the word
    if letter_ix >= len(words[start_word_ix]) and start_word_ix != end_word_ix:
        start_word_ix += 1
        if start_word_ix == end_word_ix:  # base case 2
            return root_scores, postfix_scores

    # inductive case
    prev_word_ix = start_word_ix
    next_word_ix = get_next_letter_point(words, start_word_ix, end_word_ix, letter_ix)
    while next_word_ix:
        new_root_scores, new_postfix_scores = get_root_postfix_scores(words, prev_word_ix, next_word_ix - 1,
                                                                      letter_ix + 1)
        root_scores += new_root_scores
        postfix_scores += new_postfix_scores
        prev_word_ix = next_word_ix
        next_word_ix = get_next_letter_point(words, prev_word_ix, end_word_ix, letter_ix)
    return root_scores, postfix_scores


def find_postfix(word, root, postfixes):
    if word.find(root) == 0:
        word_potential_postfix = word[len(root):]
        if not word_potential_postfix or word_potential_postfix in postfixes:
            return word_potential_postfix
    return False


def match_root_postfix(word: str, roots, postfixes, start_ix=None, end_ix=None, letter_ix=0):
    found_roots = []
    found_postfixes = []
    if letter_ix >= len(word):  # base case 1
        return found_roots, found_postfixes
    if start_ix is None:
        start_ix = 0
    if end_ix is None:
        end_ix = len(roots) - 1

    # get the first and last matching on current letter
    word_letter = word[letter_ix]
    l_ix, r_ix, cl_ix, cr_ix = start_ix, end_ix, None, None

    if len(roots[l_ix]) <= letter_ix:  # adjust in case word has no next letters
        postfix = find_postfix(word, roots[l_ix], postfixes)
        if postfix != False:
            found_roots.append(roots[l_ix])
            found_postfixes.append(postfix)
        l_ix += 1

    # adjust in case the first or last word contains the searched letter
    if roots[l_ix][letter_ix] == word_letter:
        cl_ix = cr_ix = l_ix
        l_ix -= 1
    if roots[r_ix][letter_ix] == word_letter:
        cr_ix = r_ix
        if cl_ix is None:
            cl_ix = cr_ix
        r_ix += 1

    while (not (l_ix + 1 == cl_ix and cr_ix == r_ix - 1)) and l_ix + 1 != r_ix and l_ix != r_ix:
        if cl_ix is None:
            current_ix = (l_ix + r_ix) // 2
            current_letter = roots[current_ix][letter_ix]
            if current_letter == word_letter:
                cl_ix = cr_ix = current_ix
            elif current_letter < word_letter:
                l_ix = current_ix
            else:
                r_ix = current_ix
        elif l_ix + 1 == cl_ix:
            current_ix = (cr_ix + r_ix) // 2
            current_letter = roots[current_ix][letter_ix]
            if current_letter == word_letter:
                cr_ix = current_ix
            else:
                r_ix = current_ix
        else:
            current_ix = (l_ix + cl_ix) // 2
            current_letter = roots[current_ix][letter_ix]
            if current_letter == word_letter:
                cl_ix = current_ix
            else:
                l_ix = current_ix

    if l_ix + 1 == r_ix or l_ix == r_ix:  # base case 2
        check_roots = [roots[l_ix]]
        if l_ix + 1 == r_ix:
            check_roots.append(roots[r_ix])
        for check_root in check_roots:
            postfix = find_postfix(word, check_root, postfixes)
            if postfix != False:
                found_roots.append(check_root)
                found_postfixes.append(postfix)
            return found_roots, found_postfixes
        return found_roots, found_postfixes
    if cl_ix == cr_ix:  # base case 3
        check_root = roots[cl_ix]
        postfix = find_postfix(word, check_root, postfixes)
        if postfix != False:
            found_roots.append(check_root)
            found_postfixes.append(postfix)
        return found_roots, found_postfixes
    new_roots, new_postfixes = match_root_postfix(word, roots, postfixes, cl_ix, cr_ix, letter_ix + 1)
    postfix = find_postfix(word, roots[cl_ix], postfixes)
    if postfix != False and postfix not in new_postfixes:
        found_roots.append(roots[cl_ix])
        found_postfixes.append(postfix)
    return found_roots + new_roots, found_postfixes + new_postfixes



def generate_morphemes(vocabulary_filepath, morpheme_translation_table_path):
    print("RUNNING MORPHEME GENERATION STAGE...")
    dict_file = pd.read_csv(vocabulary_filepath, index_col=0, keep_default_na=False).iloc[1:SAMPLE_SIZE]
    words = sorted(dict_file.index)

    print("generating root and postfix scores")
    root_scores, postfix_scores = get_root_postfix_scores(words, 0, len(words) - 1, 0)
    sorted_root_scores = pd.Series(root_scores).sort_index()
    sorted_postfix_scores = pd.Series(postfix_scores).sort_values(ascending=False)

    print("filtering roots and postfixes")
    filtered_postfix_scores = sorted_postfix_scores.iloc[:POSTFIX_KEEP_NUMBER].sort_index()

    print("matching words to the roots and postfixes")
    word_roots, word_postfixes = {}, {}
    for word in words:
        found_roots, found_postfixes = match_root_postfix(word, sorted_root_scores.index, filtered_postfix_scores.index)
        if found_roots:
            word_roots[word] = found_roots
            word_postfixes[word] = found_postfixes

    print("frequency scoring each root...")
    root_scores_adjusted = Counter()
    for word in word_roots:
        for root in word_roots[word]:
            root_scores_adjusted[root] += 1

    print("eliminating conflicting roots")
    for word in word_roots:
        if len(word_roots[word]) > 1:
            max_ix = 0
            max_root = None
            max_score = 0
            for ix, root in enumerate(word_roots[word]):
                score = root_scores_adjusted[root]
                if score > max_score or (score == max_score and len(root) < len(max_root)):
                    max_ix = ix
                    max_root = root
                    max_score = score
            word_roots[word] = word_roots[word][max_ix]
            word_postfixes[word] = word_postfixes[word][max_ix]
        else:
            word_roots[word] = word_roots[word][0]
            word_postfixes[word] = word_postfixes[word][0]

    print("generating translation table")
    translation_table = pd.DataFrame(
        {'word': list(word_roots.keys()), 'root': list(word_roots.values()), 'postfix': list(word_postfixes.values())})

    print("SAVING TEMPORARY FILES...")
    translation_table.to_csv(morpheme_translation_table_path, index=False)

    print("MORPHEME GENERATION COMPLETE!")
