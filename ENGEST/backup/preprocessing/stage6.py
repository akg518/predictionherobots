"""
remove line_numbers
add start_translate token and append to the end of the beginning of estonian file
add 5 unknown tokens + 1 unknown translate token
cap the sequences to top N occurrences and offset by 7
"""

from .utils import *
import numpy as np
import pandas as pd
import multiprocessing as mp
from ENGEST.config import *



# TOKEN SPECIFICATION
# 0 - null token
# 1/2 - start/end tokens
# 3-7 - english unkown tokens
# 8 unknown translate token
# 9-5009 - word tokens

def add_extra_tokens(chunk_en: pd.DataFrame(), chunk_et: pd.DataFrame(), et2en_hash_map):
    print("starting process {}".format(mp.current_process()))
    print("{} - starting type conversion".format(mp.current_process()))
    chunk_en = chunk_en.drop('line_no', axis=1).astype('int64')
    chunk_et = chunk_et.drop('line_no', axis=1).astype('int64')
    print("{} - offsetting null variable".format(mp.current_process()))
    offset_null = np.vectorize(lambda x: x - EXTRA_TOKENS if x == 0 else x)
    chunk_en = chunk_en.apply(offset_null)
    chunk_et = chunk_et.apply(offset_null)

    unkown_word_low_bound = 3 - EXTRA_TOKENS
    unkown_word_high_bound = 3 - EXTRA_TOKENS + NO_UNKNOWN
    print("{} - capping and translating dictionaries".format(mp.current_process()))
    update_status_number = 10
    no_rows = len(chunk_en.index)
    update_rows = [ix for ix in range(0, no_rows, no_rows // update_status_number)]
    for row_ix in range(len(chunk_et.index)):
        if update_rows and row_ix == update_rows[0]:
            print("{} - {}% complete...".format(mp.current_process(), 100 * (
                        update_status_number - len(update_rows)) // update_status_number))
            del update_rows[0]
        # get all of the english unknown hashes
        english_unknown_hashes = list(filter(lambda x: x > WORD_CAP, chunk_en.iloc[row_ix]))

        # cap and translate estonian unknown words
        for col_ix in range(len(chunk_et.columns)):
            cell = chunk_et.iloc[row_ix, col_ix]
            if cell > WORD_CAP:
                if cell in et2en_hash_map and et2en_hash_map[cell] in english_unknown_hashes:
                    hash_index = english_unknown_hashes.index(et2en_hash_map[cell])
                    chunk_et.iloc[row_ix, col_ix] = hash_index - NO_UNKNOWN
                else:
                    chunk_et.iloc[row_ix, col_ix] = 0
            if cell == -1 * EXTRA_TOKENS or col_ix == len(chunk_et.columns) - 1:
                chunk_et.iloc[row_ix, col_ix] = 2-EXTRA_TOKENS
                break

        # cap and translate english words
        current_unknown_word = unkown_word_low_bound
        for col_ix in range(len(chunk_en.columns)):
            cell = chunk_en.iloc[row_ix, col_ix]
            if cell > WORD_CAP:
                if current_unknown_word > unkown_word_high_bound:
                    chunk_en.iloc[row_ix, col_ix] = 0
                else:
                    chunk_en.iloc[row_ix, col_ix] = current_unknown_word
                    current_unknown_word += 1
                if cell == -1 * EXTRA_TOKENS or col_ix == len(chunk_en.columns) - 1:
                    break
    print("{} - offsetting full dictionaries and reverting types".format(mp.current_process()))
    offset_all = np.vectorize(lambda x: x + EXTRA_TOKENS)
    chunk_en = chunk_en.apply(offset_all).astype('uint16')
    chunk_et = chunk_et.apply(offset_all).astype('uint16')
    chunk_et.insert(0, 'start_token', 1)
    print("{} - process complete!".format(mp.current_process()))
    return chunk_en, chunk_et


def run_stage_6():
    print("RUNNING STAGE 6")
    files_required_exist = all(os.path.isfile(path) for path in
                               [PARQUET_FILE_ET.format(5), PARQUET_FILE_EN.format(4), DICT_FILE_EN, DICT_FILE_ET])
    if not files_required_exist:
        print("ONE OR MORE STAGE PREVIOUS STAGE FILES MISSING!")
        exit(1)

    print("LOADING FILES")
    df_en_chunks = load_parquet_in_chunks(PARQUET_FILE_EN.format(4), STAGE_6_CHUNK_SIZE, SAMPLE_SIZE)
    df_et_chunks = load_parquet_in_chunks(PARQUET_FILE_ET.format(5), STAGE_6_CHUNK_SIZE, SAMPLE_SIZE)
    dict_en = pd.read_csv(DICT_FILE_EN, index_col=0, keep_default_na=False)
    dict_et = pd.read_csv(DICT_FILE_ET, index_col=0, keep_default_na=False)

    print("dictionaries have been split into {} chunks.".format(len(df_en_chunks)))

    print("GENERATING HASH MAPS")
    # this shows which english hashes are the same as estonian hashes
    enet_hashmap = dict()
    for en_word in dict_en.index:
        if en_word in dict_et.index:
            enet_hashmap[dict_en.at[en_word, 'hash']] = dict_et.at[en_word, 'hash']
    eten_hashmap = dict(zip(enet_hashmap.keys(), enet_hashmap.values()))
    print("SAVING HASH MAP")
    pd.Series(enet_hashmap).to_csv(HASH_MAPPING, index_label='en_hash')

    print("CAPPING AND CONVERTING CHUNKS!")
    with mp.Pool(THREAD_POOL_SIZE) as p:
        capped_chunks = p.starmap(add_extra_tokens, ((df_en_chunks[i], df_et_chunks[i], eten_hashmap) for i in range(len(df_en_chunks))))
    en_capped_chunks, et_capped_chunks = zip(*capped_chunks)
    del capped_chunks
    df_en_capped = pd.concat(en_capped_chunks)
    del en_capped_chunks
    df_et_capped = pd.concat(et_capped_chunks)
    del et_capped_chunks

    print("SAVING FILES")
    df_en_capped.to_parquet(PARQUET_FILE_EN.format(6))
    df_et_capped.to_parquet(PARQUET_FILE_ET.format(6))