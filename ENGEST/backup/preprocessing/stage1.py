"""generate statistics about the raw text files"""
import collections
from .utils import *
from ENGEST.config import *


def get_stats(lines, morpheme_translation_table_path=None):
    """
    generate statistics about the text file, namely the maximum number of words,
    the maximum numer of characters in a line and a maximum number of characters in a word
    :param lines: array of lines of the text file
    :return: max_words, max_line, max_word
    """
    max_line = 0
    max_word = 0
    word_frequencies = collections.Counter()
    lines_num = len(lines)

    translate_df = None
    if morpheme_translation_table_path:
        translate_df = pd.read_csv(morpheme_translation_table_path, index_col='word', keep_default_na=False)

    # traversing through the lines to generate stats
    for line in lines:
        line_len = len(line)
        words = preprep_line(line, translate_df)
        words_num = len(words)
        if not words:
            line_max_word = 0
        else:
            line_max_word = max(map(len, words))
        word_frequencies[words_num] += 1
        if line_len > max_line:
            max_line = line_len
        if line_max_word > max_word:
            max_word = line_max_word

    # calculating percentile metrics
    percentiles = [1, 0.99, 0.95, 0.9, 0.8]
    percentile_result = [None for _ in range(len(percentiles))]
    r_traversed = lines_num
    percentile_ranks = [int(percentile * lines_num) for percentile in percentiles]
    percentile_ix = 0
    r_sorted_word_freq = sorted(word_frequencies)[-1::-1]
    word_ix = 0
    while percentile_ix < len(percentile_ranks):
        if r_traversed <= percentile_ranks[percentile_ix]:
            percentile_result[percentile_ix] = r_sorted_word_freq[word_ix]
            percentile_ix += 1
        else:
            r_traversed -= word_frequencies[r_sorted_word_freq[word_ix]]
            word_ix += 1

    # create a return structure and return
    result = {'max_line': max_line, 'max_word': max_word}
    result.update({'{}%'.format(int(percentiles[i] * 100)): percentile_result[i] for i in range(len(percentiles))})
    return result


def run_stage_1(use_morpheme_translation=USE_MORPHEME_TRANSLATION_TABLE):
    print("STARTING STAGE 1 PRE-PROCESSING...")
    out_vars = {}

    # english file
    lines = get_lines(RAW_FILE_EN)[:SAMPLE_SIZE]
    if use_morpheme_translation:
        en_stats = get_stats(lines, MORPHEME_TRANSLATION_TABLE_EN)
    else:
        en_stats = get_stats(lines)
    out_vars['en'] = en_stats
    print("english file," + ', '.join("{}: {}".format(key, value) for key, value in en_stats.items()))

    # estonian file
    lines = get_lines(RAW_FILE_ET)[:SAMPLE_SIZE]
    if use_morpheme_translation:
        et_stats = get_stats(lines, MORPHEME_TRANSLATION_TABLE_ET)
    else:
        et_stats = get_stats(lines)
    out_vars['et'] = et_stats
    print("estonian file," + ', '.join("{}: {}".format(key, value) for key, value in et_stats.items()))

    print("updating the variable file")
    update_vars(VARS_FILE, out_vars)
    print("STAGE 1 COMPLETE!")
