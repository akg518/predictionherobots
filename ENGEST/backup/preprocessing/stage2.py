"""read the raw file, do per line pre-processing and save as parquet"""

from .utils import *
from ENGEST.config import *
import pandas as pd
import numpy as np


def convert_to_parquet(lines, words_limit, out_path, use_morpheme_translation, translate_path=None):
    """
    convert the lines file to pandas dataframe and save as a parquet file
    :param lines: the lines object (array of strings)
    :param words_limit: limit of words per file (for memory reasons)
    :param out_path: the output path of the parquet file
    :param translate_path: the morpheme translation table
    :param use_morpheme_translation: whehter to convert words into morpheme constituents
    """
    lines_encountered = set()
    series_list = []
    chunk_size = STAGE_1_CHUNK_SIZE
    chunk_no = 0
    df = pd.DataFrame()
    translate_df = None
    if use_morpheme_translation:
        translate_df = pd.read_csv(translate_path, index_col='word', keep_default_na=False)
    initial_line_num = len(lines)
    print("generating the dataframe:")
    while lines:
        lines_left = len(lines)
        print("processing chunk {}".format(chunk_no))
        print("lines left: {}".format(lines_left))
        lines_to_process = min(chunk_size, lines_left)
        for ix in range(lines_to_process):
            line = lines[ix]
            if line not in lines_encountered:
                lines_encountered.add(line)
                line_prepreped = preprep_line(line, translate_df)
                line_word_num = len(line_prepreped)
                if line_word_num == 0 or line_word_num > words_limit:
                    continue
                nan_num = words_limit - line_word_num
                series_list.append(pd.Series([chunk_no * chunk_size + ix] + line_prepreped + [np.nan] * nan_num))
        df = pd.concat([df] + series_list[:chunk_size], axis=1, ignore_index=True)
        chunk_no += 1
        del series_list[:]
        del lines[:lines_to_process]
    print("transposing and finishing off the matrix")
    df = df.T
    df.columns = ['line_no'] + ["w_{}".format(i) for i in range(words_limit)]
    df = df.astype({'line_no': 'int32'})
    print("concatentation complete!")
    print("final line number: {} (lines removed: {})".format(len(df.index), initial_line_num - len(df.index)))
    print("saving to a parquet file...")
    df.to_parquet(out_path, engine='pyarrow')


def run_stage_2(use_morpheme_translation=USE_MORPHEME_TRANSLATION_TABLE):
    print("STARTING STAGE 2 PRE-PROCESSING...")
    preprep_vars = get_vars(VARS_FILE, {'en': {''}})
    if preprep_vars == False:
        print("PLEASE RERUN STAGE 1!")
        exit(1)
    print("CONVERTING THE TXT FILE TO loader DATAFRAME")
    lines = get_lines(RAW_FILE_EN)[:SAMPLE_SIZE]
    convert_to_parquet(lines, preprep_vars['en'][KEEP_PERCENTAGE_LENGTH], PARQUET_FILE_EN.format(2), use_morpheme_translation,
                       MORPHEME_TRANSLATION_TABLE_EN)
    del lines
    print("STAGE 2 COMPLETE!")
