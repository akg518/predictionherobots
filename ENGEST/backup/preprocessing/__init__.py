from .stage1 import run_stage_1
from .stage2 import run_stage_2
from .stage3 import run_stage_3
from .stage4 import run_stage_4
from .stage5 import run_stage_5
from .stage6 import run_stage_6

runner_map = {1: run_stage_1,
              2: run_stage_2,
              3: run_stage_3,
              4: run_stage_4,
              5: run_stage_5,
              6: run_stage_6}

STAGES_WITH_MORPHEME_GENERATION = [1, 2, 4]


def run_stage(stage, use_morpheme_translation_table=None):
    if stage in STAGES_WITH_MORPHEME_GENERATION and use_morpheme_translation_table is not None:
        return runner_map[stage](use_morpheme_translation_table)
    return runner_map[stage]()
