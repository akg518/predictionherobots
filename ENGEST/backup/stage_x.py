"""
large estonian dictionary size is attributed to root word differentiation.
To reduce the number of the dictionary automatic morpheme identification algorithm will be created below.
"""

# morpheme identification is given in 5 steps:
# 0) sort all of the words and bucket into word lengths
# 1) identify all the potential root words and label all of the times where the word is root
# 2) estimate the score of all of the roots, as based on how frequent their prefixes are
# 3) estimate the score of the prefixes by checking how well it fits with the roots
# 4) re-estimate the score of the roots to see how well it fits with the prefixes
# ???
# profit

from collections import Counter
import pandas as pd
import pickle
from ENGEST.config import *

RUNSTEPS = [2, 3]
ROOTS_FILE = 'roots.pkl'
PREFIX_FILE = 'prefixes.pkl'
POSTFIX_FILE = 'postfixes.pkl'
PREFIX_TOTAL_FILE = 'prefixes_total.pkl'
POSTFIX_TOTAL_FILE = 'postfixes_total.pkl'
ROOT_SCORES_FILE = 'root_scores.csv'


def append_dict_list(dict_list, index, value):
    if index not in dict_list:
        dict_list[index] = []
    dict_list[index].append(value)


def has_root(word: str, root: str):
    return root in word


def get_root_indexes(word: str, root: str):
    indexes = []
    root_start = word.find(root)
    while root_start != -1:
        root_end = root_start + len(root)
        indexes.append((root_start, root_end))
        root_start = word.find(root, root_end)
    return indexes


# STEP 0 (compulsory data loads!)
dict_en = pd.read_csv(DICT_FILE_EN, index_col=0, keep_default_na=False).iloc[1:SAMPLE_SIZE]

print("GENERATING SORTED WORDS")
words = sorted(dict_en.index, key=len)
len2index = {}  # shows the earliest index of that word length
look_for_length = 1
max_word_length = len(words[-1])
ix = 0
while look_for_length <= max_word_length:
    word_len = len(words[ix])
    if word_len >= look_for_length:
        len2index[word_len] = ix
        look_for_length = word_len + 1
    ix += 1

index2len = dict(zip(len2index.values(), len2index.keys()))

print("ADDING MISSING WORD LENGTH BUCKETS")
for word_len in range(max_word_length, 1, -1):
    if word_len not in len2index:
        len2index[word_len] = len2index[word_len + 1]

freq = dict_en['freq']
potential_roots, potential_prefixes, potential_postfixes = {}, {}, {}
prefix_counter, postfix_counter = Counter(), Counter()

if 1 in RUNSTEPS:
    print("RUNNING STEP 1...")
    biggest_possible_root = len2index[max_word_length] - 1
    for ix, root_word in enumerate(words[:biggest_possible_root]):

        # status updates
        if ix in index2len:
            print("processing word length {}..".format(index2len[ix]))
        root_len = len(root_word)
        smallest_possible_word = root_len + 1

        for full_word in words[len2index[smallest_possible_word]:]:
            if has_root(full_word, root_word):
                # add prefix and suffix to the potential morpheme list
                indexes = get_root_indexes(full_word, root_word)
                for index in indexes:
                    prefix = full_word[:index[0]]
                    postfix = full_word[index[1]:]
                    if prefix:
                        append_dict_list(potential_prefixes, prefix, full_word)
                        prefix_counter[prefix] += freq[full_word]
                    if postfix:
                        append_dict_list(potential_postfixes, postfix, full_word)
                        postfix_counter[postfix] += freq[full_word]

                # append the full word to the potential root list
                append_dict_list(potential_roots, root_word, full_word)

    print("SAVING TEMPORARY FILES...")
    for filepath, obj in [(ROOTS_FILE, potential_roots),
                          (PREFIX_FILE, potential_prefixes),
                          (POSTFIX_FILE, potential_postfixes),
                          (PREFIX_TOTAL_FILE, prefix_counter),
                          (POSTFIX_TOTAL_FILE, postfix_counter)]:
        with open(filepath, 'wb') as f:
            pickle.dump(obj, f)

    print("STAGE 1 COMPLETE!")


def calc_root_affix_word_ratio(word, root, prefix_words, postfix_words, word_freq):
    """
    root affix ratio calculates how well the given root splits the word into
    meaningful blocks of prefix_root_postfix
    :return: minimum 1, maximum depending on the frequency of affixes
    """
    indexes = get_root_indexes(word, root)
    affix_no = 0
    total_affix_frequency = 0
    for index in indexes:
        prefix = word[:index[0]]
        postfix = word[index[1]:]
        if prefix:
            affix_no += 1
            total_affix_frequency += len(prefix_words[prefix])
        if postfix:
            affix_no += 1
            total_affix_frequency += len(postfix_words[postfix])
    return total_affix_frequency / affix_no


def calc_root_affix_ratio(root, root_words, prefix_words, postfix_words, word_freq):
    """
    a.k.a root efficacy score, its the weighted result of root affix word ratios over all the words
    :return: 1 minimum (lowest score possible)
    """
    word_weights = {word: word_freq[word] for word in root_words}
    total_weight = sum(word_weights.values())
    weighted_sum = 0
    for word in root_words:
        weighted_sum += calc_root_affix_word_ratio(word, root, prefix_words, postfix_words, word_freq) * word_weights[
            word]
    weighted_sum /= total_weight
    return weighted_sum


if 2 in RUNSTEPS:

    print("RUNNING STAGE 2")
    print("LOADING VALUES...")
    with open(ROOTS_FILE, 'rb') as f:
        potential_roots = pickle.load(f)
    with open(PREFIX_FILE, 'rb') as f:
        potential_prefixes = pickle.load(f)
    with open(POSTFIX_FILE, 'rb') as f:
        potential_postfixes = pickle.load(f)
    with open(PREFIX_TOTAL_FILE, 'rb') as f:
        prefix_counter = pickle.load(f)
    with open(POSTFIX_TOTAL_FILE, 'rb') as f:
        postfix_counter = pickle.load(f)

    print("SETTING UP NEW VARIABLES")
    root_scores = {}
    update_status_number = 20
    no_roots = len(potential_roots)
    update_status_roots = [list(potential_roots.keys())[ix] for ix in
                           range(0, no_roots, no_roots // update_status_number)]

    print("CALCULATING ROOT SCORES...")
    for root in potential_roots:
        if update_status_roots and root == update_status_roots[0]:
            print('{}% complete'.format(100 / update_status_number * (1 + update_status_number - len(update_status_roots))))
            del update_status_roots[0]
        root_scores[root] = calc_root_affix_ratio(root, potential_roots[root], potential_prefixes, potential_postfixes,
                                                  freq)

    print("NORMALISING SCORES and sorting roots")
    root_scores = pd.Series(root_scores).sort_values(ascending=False)
    root_scores /= root_scores.iloc[0]

    print("SAVING RESULTS")
    root_scores.to_csv(ROOT_SCORES_FILE)

    print("STAGE 2 COMPLETE!")


def calc_affix_word_score(affix, affix_type, word, word2roots, root_scores):
    # get all the roots for the word
    roots = word2roots[word]
    # filter for only the roots that are used with the affix
    if affix_type == "prefix":
        affix_root_found = lambda root: word.find(affix + root) == 0

    elif affix_type == "postfix":
        affix_root_found = lambda root: word.rfind(root + affix) != -1 \
                                        and word.rfind(root + affix) == len(word) - len(root) - len(affix)
    else:
        raise ValueError("incorrect affix type provided!")
    valid_roots = [root for root in roots if affix_root_found(root)]
    scores = [root_scores[valid_root] for valid_root in valid_roots]
    return sum(scores) / len(scores)


def calc_affix_score(affix, affix_type, affix2words, word2roots, root_scores, word_freq):
    words = affix2words[affix]
    map_funct = lambda word: calc_affix_word_score(affix, affix_type, word, word2roots, root_scores)
    scores = list(map(map_funct, words))
    word_weights = list(map(word_freq.__getitem__, words))
    scores_weighted = sum(scores[ix] * word_weights[ix] for ix in range(len(scores))) / sum(word_weights)
    return scores_weighted


if 3 in RUNSTEPS:
    print("STARTING STAGE 3..")
    print("generating word root mapping")
    word_root_map = {}
    for root in potential_roots:
        for word in potential_roots[root]:
            append_dict_list(word_root_map, word, root)

    print("checking affix scores")
    prefix_scores = {}
    postfix_scores = {}
    for affix_dict, affix_score_dict, affix_type in [(potential_prefixes, prefix_scores, 'prefix'),
                                                     (potential_postfixes, postfix_scores, 'postfix')]:
        for affix_key in affix_dict.keys():
            affix_score_dict[affix_key] = calc_affix_score(affix_key, affix_type, affix_dict, word_root_map,
                                                           root_scores, freq)
    print("normalising scores:")
    print("saving data")
    print("STARTING STAGE 3..")
