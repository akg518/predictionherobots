"""generate training and test sets"""

# only thing needed to do here is to generate an one-hot encoding for the output file


# DO NOT USE AT THE MOMENT!! THE FILES ARE WAY TOO BIG TO BE USED

def run_stage_7():
    pass
#     print("RUNNING STAGE 7...")
#     if os.path.isfile(OUTPUT_FILE):
#         print("removing old files...")
#         os.remove(OUTPUT_FILE)
#     print("loading estonian file...")
#     df_et = pd.read_parquet(PARQUET_FILE_ET.format(6))
#     max_sentence_length = len(df_et.columns)-1
#     no_sentences = len(df_et.index)
#     no_tokens = WORD_CAP + EXTRA_TOKENS
#     print("generating 3d array...")
#     one_hot_array = np.zeros(shape=(no_sentences, max_sentence_length, no_tokens), dtype=np.int8)
#     print("filling array with tokens...")
#     for row_ix in range(len(df_et.index)):
#             for col_ix in range(len(df_et.columns)-1):
#                 token_ix = df_et.iloc[row_ix, col_ix+1]  # offset by one to remove start token
#                 if token_ix:
#                     one_hot_array[row_ix, col_ix, token_ix-1] = 1
#     print("saving file...")
#     np.save(OUTPUT_FILE, one_hot_array)
#     print("STAGE 7 COMPLETE!")
