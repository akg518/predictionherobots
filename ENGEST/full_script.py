import sys
import datetime
import random
import os
import json
import re
import multiprocessing as mp
import pandas as pd
import numpy as np
from collections import Counter
from keras import Model, Input, optimizers
from keras.callbacks import ModelCheckpoint
from keras.engine.saving import load_model
from keras.layers import Dense, LSTM, Embedding


# CHANGING THE WORKING DIRECTORY
if len(sys.argv) != 2:
    print("please insert the working directory in the script parameter!")
    #exit(1)

WORKING_DIR = sys.argv[1]
sys.path.append(WORKING_DIR)

os.chdir(WORKING_DIR)

# CONFIGURATIONS

MANUAL_RUN_STAGES = []  # insert numbers corresponding to stages that you want to run

random.seed(38710)

DATA_DIR = WORKING_DIR + 'DATA/ENGEST/'
BACKUP_DIR = DATA_DIR + 'BACKUP/'

RAW_FILE_EN = DATA_DIR + "europarl-v7.et-en.en"
RAW_FILE_ET = DATA_DIR + "europarl-v7.et-en.et"
RAW_FILES = ['europarl-v7.et-en.en', 'europarl-v7.et-en.et']
VARS_FILE = DATA_DIR + "vars.json"
PARQUET_FILE_EN = DATA_DIR + "en_STAGE{}.parquet"
PARQUET_FILE_ET = DATA_DIR + "et_STAGE{}.parquet"
DICT_FILE_EN = DATA_DIR + "en_dict.csv"
DICT_FILE_ET = DATA_DIR + "et_dict.csv"
HASH_MAPPING = DATA_DIR + "hash_mapping.csv"
MORPHEME_TRANSLATION_TABLE_EN = DATA_DIR + "morpheme_translate_en.csv"
MORPHEME_TRANSLATION_TABLE_ET = DATA_DIR + "morpheme_translate_et.csv"
TEMP_MODEL_WEIGHTS_FILEPATH = DATA_DIR + "weights.best.hdf5"
FINAL_MODEL_FILEPATH = DATA_DIR + "model_final.hdf5"

# used for preprocessing files
KEEP_PERCENTAGE_LENGTH = "95%"  # this is for dropping some very long sequences

# used for multiprocessing
STAGE_1_CHUNK_SIZE = 10000
STAGE_2_CHUNK_SIZE = 50000
STAGE_6_CHUNK_SIZE = 10000
THREAD_POOL_SIZE = 4

# used for generating a morpheme translation table
MIN_ROOT_LENGTH = 3
POSTFIX_KEEP_NUMBER = 50

# this is used to convert individual words into a sequence of morphemes (only at preprocessing stages)
USE_MORPHEME_TRANSLATION_TABLE = True

# used for capping the vocabulary (stage 6)
NO_UNKNOWN = 5  # number of unknown english tokens
WORD_CAP = 5000  # max number of tokens
EXTRA_TOKENS = NO_UNKNOWN + 2 + 1  # 2 for start/end, 1 for unknown translate token

SAMPLE_SIZE = -1  # used for testing on the smaller corpus, use -1 if parsing full corpus

# TRAINING VARIABLES
VALIDATE_SET = 0.01
VALIDATE_OFFSET = random.randint(0, 95 - (100 * VALIDATE_SET)) / 100
LATENT_DIM = 256
EPOCH_NO = 15
STEPS_PER_EPOCH = 100
BATCH_SIZE = 32
NO_TOKENS = WORD_CAP + EXTRA_TOKENS + 1


# UTILITY FUNCTIONS
def get_vars(vars_filepath, vars=None):
    """helper function, checks if all the necessary variables are in the vars file"""
    if not os.path.isfile(vars_filepath):
        print("json file does not exist, quitting now...")
        return False
    if vars is not None:
        with open(vars_filepath) as f:
            try:
                file_vars = json.load(f)
                if type(file_vars) is not dict:
                    raise json.decoder.JSONDecodeError
            except json.decoder.JSONDecodeError:
                print("json file corrupted, quitting now...")
                return False
            missing_vars = set(vars) - set(file_vars.keys())
            if missing_vars:
                print("variables are missing! {}".format(missing_vars))
                return False
            return file_vars


def update_vars(vars_filepath, new_vars):
    # create file if it doesn't exist
    if not os.path.isfile(vars_filepath):
        open(vars_filepath, 'w+').close()
    # read the contents first and save in memory
    with open(vars_filepath, 'r') as f:
        try:
            temp_vars = json.load(f)
            if type(temp_vars) is not dict:
                raise json.decoder.JSONDecodeError
        except json.decoder.JSONDecodeError:
            temp_vars = {}
    # update and save the new variables, overriding the previous content
    temp_vars.update(new_vars)
    with open(vars_filepath, 'w') as f:
        json.dump(temp_vars, f)


def get_lines(filename):
    with open(filename, 'r', encoding="utf8") as f:
        return f.readlines()


def separate_characters(line):
    """
    separating long numbers and punctuation into individual words
    :param line: the line to be converted
    :return: string with spaces added in the right place
    """
    SEP_LETTERS = "1234567890,.;:?!\-&\{\[\(\}\]\)\\\"\'/”"
    return re.sub(r'([{}])'.format(SEP_LETTERS), r' \1 ', line)


def translate_tokens(words, translate_df):
    translated = []
    translate_index = translate_df.index
    for word in words:
        if word in translate_index:
            translated.append(translate_df.at[word, 'root'])
            postfix = translate_df.at[word, 'postfix']
            if not pd.isnull(postfix):
                translated.append(postfix)
        else:
            translated.append(word)
    return translated


def preprep_line(line, translate_df=None):
    """
    line pre-preparation, splits up the words and returns a list of words
    :param line: the line to do preprep on
    :param translate_df: if some words are to be split up then this file provides the translation
    :return: list of strings
    """
    if translate_df is not None:
        return translate_tokens(separate_characters(line).lower().strip().split(), translate_df)
    return separate_characters(line).lower().strip().split()


def load_parquet_in_chunks(in_parquet, chunk_size, sample_size):
    df = pd.read_parquet(in_parquet).iloc[:sample_size, :]
    no_chunks = len(df) // chunk_size
    if len(df) % chunk_size != 0:
        no_chunks += 1
    return [df.iloc[i * chunk_size: min((i + 1) * chunk_size, len(df)), :] for i in range(no_chunks)]


# STAGE 1
def get_stats(lines, morpheme_translation_table_path=None):
    """
    generate statistics about the text file, namely the maximum number of words,
    the maximum numer of characters in a line and a maximum number of characters in a word
    :param lines: array of lines of the text file
    :return: max_words, max_line, max_word
    """
    max_line = 0
    max_word = 0
    word_frequencies = Counter()
    lines_num = len(lines)

    translate_df = None
    if morpheme_translation_table_path:
        translate_df = pd.read_csv(morpheme_translation_table_path, index_col='word', keep_default_na=False)

    # traversing through the lines to generate stats
    for line in lines:
        line_len = len(line)
        words = preprep_line(line, translate_df)
        words_num = len(words)
        if not words:
            line_max_word = 0
        else:
            line_max_word = max(map(len, words))
        word_frequencies[words_num] += 1
        if line_len > max_line:
            max_line = line_len
        if line_max_word > max_word:
            max_word = line_max_word

    # calculating percentile metrics
    percentiles = [1, 0.99, 0.95, 0.9, 0.8]
    percentile_result = [None for _ in range(len(percentiles))]
    r_traversed = lines_num
    percentile_ranks = [int(percentile * lines_num) for percentile in percentiles]
    percentile_ix = 0
    r_sorted_word_freq = sorted(word_frequencies)[-1::-1]
    word_ix = 0
    while percentile_ix < len(percentile_ranks):
        if r_traversed <= percentile_ranks[percentile_ix]:
            percentile_result[percentile_ix] = r_sorted_word_freq[word_ix]
            percentile_ix += 1
        else:
            r_traversed -= word_frequencies[r_sorted_word_freq[word_ix]]
            word_ix += 1

    # create a return structure and return
    result = {'max_line': max_line, 'max_word': max_word}
    result.update({'{}%'.format(int(percentiles[i] * 100)): percentile_result[i] for i in range(len(percentiles))})
    return result


def run_stage_1(use_morpheme_translation=USE_MORPHEME_TRANSLATION_TABLE):
    print("STARTING STAGE 1 PRE-PROCESSING...")
    out_vars = {}

    # english file
    lines = get_lines(RAW_FILE_EN)[:SAMPLE_SIZE]
    if use_morpheme_translation:
        en_stats = get_stats(lines, MORPHEME_TRANSLATION_TABLE_EN)
    else:
        en_stats = get_stats(lines)
    out_vars['en'] = en_stats
    print("english file," + ', '.join("{}: {}".format(key, value) for key, value in en_stats.items()))

    # estonian file
    lines = get_lines(RAW_FILE_ET)[:SAMPLE_SIZE]
    if use_morpheme_translation:
        et_stats = get_stats(lines, MORPHEME_TRANSLATION_TABLE_ET)
    else:
        et_stats = get_stats(lines)
    out_vars['et'] = et_stats
    print("estonian file," + ', '.join("{}: {}".format(key, value) for key, value in et_stats.items()))

    print("updating the variable file")
    update_vars(VARS_FILE, out_vars)
    print("STAGE 1 COMPLETE!")


# STAGE 2
def convert_to_parquet(lines, words_limit, out_path, use_morpheme_translation, translate_path=None):
    """
    convert the lines file to pandas dataframe and save as a parquet file
    :param lines: the lines object (array of strings)
    :param words_limit: limit of words per file (for memory reasons)
    :param out_path: the output path of the parquet file
    :param translate_path: the morpheme translation table
    :param use_morpheme_translation: whehter to convert words into morpheme constituents
    """
    lines_encountered = set()
    series_list = []
    chunk_size = STAGE_1_CHUNK_SIZE
    chunk_no = 0
    df = pd.DataFrame()
    translate_df = None
    if use_morpheme_translation:
        translate_df = pd.read_csv(translate_path, index_col='word', keep_default_na=False)
    initial_line_num = len(lines)
    print("generating the dataframe:")
    while lines:
        lines_left = len(lines)
        print("processing chunk {}".format(chunk_no))
        print("lines left: {}".format(lines_left))
        lines_to_process = min(chunk_size, lines_left)
        for ix in range(lines_to_process):
            line = lines[ix]
            if line not in lines_encountered:
                lines_encountered.add(line)
                line_prepreped = preprep_line(line, translate_df)
                line_word_num = len(line_prepreped)
                if line_word_num == 0 or line_word_num > words_limit:
                    continue
                nan_num = words_limit - line_word_num
                series_list.append(pd.Series([chunk_no * chunk_size + ix] + line_prepreped + [np.nan] * nan_num))
        df = pd.concat([df] + series_list[:chunk_size], axis=1, ignore_index=True)
        chunk_no += 1
        del series_list[:]
        del lines[:lines_to_process]
    print("transposing and finishing off the matrix")
    df = df.T
    df.columns = ['line_no'] + ["w_{}".format(i) for i in range(words_limit)]
    df = df.astype({'line_no': 'int32'})
    print("concatentation complete!")
    print("final line number: {} (lines removed: {})".format(len(df.index), initial_line_num - len(df.index)))
    print("saving to a parquet file...")
    df.to_parquet(out_path, engine='pyarrow')


def run_stage_2(use_morpheme_translation=USE_MORPHEME_TRANSLATION_TABLE):
    print("STARTING STAGE 2 PRE-PROCESSING...")
    preprep_vars = get_vars(VARS_FILE, {'en': {''}})
    if preprep_vars == False:
        print("PLEASE RERUN STAGE 1!")
        exit(1)
    print("CONVERTING THE TXT FILE TO loader DATAFRAME")
    lines = get_lines(RAW_FILE_EN)[:SAMPLE_SIZE]
    convert_to_parquet(lines, preprep_vars['en'][KEEP_PERCENTAGE_LENGTH], PARQUET_FILE_EN.format(2),
                       use_morpheme_translation,
                       MORPHEME_TRANSLATION_TABLE_EN)
    del lines
    print("STAGE 2 COMPLETE!")


# STAGE 3
def calc_word_freq_chunk(chunk: pd.DataFrame):
    print("starting process {}...".format(mp.current_process()))
    counted = Counter()
    index = chunk.index
    columns = chunk.columns
    no_rows = len(index)
    status_update_number = 10  # how many times to update the status of the process
    status_update_rows = [no_rows // status_update_number * i + index[0] for i in range(status_update_number)]
    status_ix = 0
    status_update_row = status_update_rows[status_ix]
    for row_ix in index:
        if row_ix == status_update_row:
            print("{} - {:.2f}% complete...".format(mp.current_process(), 100 / status_update_number * status_ix))
            status_ix += 1
            if status_ix < status_update_number:
                status_update_row = status_update_rows[status_ix]
        for col_ix in columns[1:]:
            counted[chunk.at[row_ix, col_ix]] += 1
    print("process {} finished!".format(mp.current_process()))
    return counted


def calc_word_freq(in_parquet, dict_file):
    print("deleting old files")
    if os.path.isfile(dict_file):
        os.remove(dict_file)

    print("loading parquet file in chunks")
    chunks = load_parquet_in_chunks(in_parquet, STAGE_2_CHUNK_SIZE, SAMPLE_SIZE)

    print("calculating word frequencies...")
    print("file has been split into {} chunks".format(len(chunks)))
    print("processing file in thread pool with {} workers".format(THREAD_POOL_SIZE))

    with mp.Pool(THREAD_POOL_SIZE) as p:
        counted = sum(p.map(calc_word_freq_chunk, chunks), Counter())
    print("processing complete!")
    print("saving dictionary to csv file")

    index, freq = zip(*counted.most_common())
    en_dict = pd.DataFrame({'hash': range(len(counted)), 'freq': freq}, index=index)
    en_dict.to_csv(dict_file)


def tokenize_chunk(chunk_df, word_hashes):
    index = chunk_df.index
    columns = chunk_df.columns[1:]  # drop line_no

    no_rows = len(index)
    status_update_number = 20  # how many times to update the status of the process
    status_update_rows = [no_rows // status_update_number * i + index[0] for i in range(status_update_number)]
    status_ix = 0
    status_update_row = status_update_rows[status_ix]

    first_line = chunk_df['line_no'].iloc[0]
    last_line = chunk_df['line_no'].iloc[-1]

    print('tokenizing lines {}/{}'.format(first_line, last_line))
    for row_ix in index:
        if row_ix == status_update_row:
            print("{}/{} - {:.2f}% complete...".format(chunk_df.at[row_ix, 'line_no'], last_line,
                                                       100 / status_update_number * status_ix))
            status_ix += 1
            if status_ix < status_update_number:
                status_update_row = status_update_rows[status_ix]
        for col_ix in columns:
            value = chunk_df.at[row_ix, col_ix]
            if value is None:
                chunk_df.at[row_ix, col_ix] = 0
            else:
                chunk_df.at[row_ix, col_ix] = word_hashes[value]
    print('{}/{} - tokenizing complete!'.format(first_line, last_line))
    return chunk_df


def tokenize_file(in_parquet, out_parquet, dict_file):
    """
    take a parquet file from stage 1, tokenize it and save it
    :param in_parquet: the filepath to the stage 1 parquet file
    :param out_parquet: the output file to parquet 2 file
    :param dict_file: the dictionary file to be saved (to convert back)
    """
    print("deleting old files")
    if os.path.isfile(out_parquet):
        os.remove(out_parquet)

    print("loading parquet file in chunks")
    chunks = load_parquet_in_chunks(in_parquet, STAGE_2_CHUNK_SIZE, SAMPLE_SIZE)

    print("loading the dict file")
    df_dict = pd.read_csv(dict_file, index_col=0, keep_default_na=False)
    word_hashes = df_dict['hash'][1:]
    print("dict: {}".format(dict))
    print("Tokenizing file")
    print("File has been split into {} chunks".format(len(chunks)))
    print("Processing file in thread pool with {} workers".format(THREAD_POOL_SIZE))
    with mp.Pool(THREAD_POOL_SIZE) as p:
        df = pd.concat(p.starmap(tokenize_chunk, ((chunk, word_hashes) for chunk in chunks)))
    print("Tokenisation complete!")

    print("Saving tokenized file...")
    df = df.astype('uint32')
    df.to_parquet(out_parquet)


def run_stage_3():
    if not os.path.isfile(PARQUET_FILE_EN.format(2)):
        print("STAGE 2 FILE NOT FOUND, QUITTING NOW!")
        exit(1)
    print("STARTING STAGE 3 PRE-PROCESSING...")
    calc_word_freq(PARQUET_FILE_EN.format(2), DICT_FILE_EN)
    tokenize_file(PARQUET_FILE_EN.format(2), PARQUET_FILE_EN.format(3), DICT_FILE_EN)
    print("STAGE 3 COMPLETE!")


# STAGE 4
def run_stage_4(use_morpheme_translation=USE_MORPHEME_TRANSLATION_TABLE):
    print("RUNNING STAGE 4")
    print("CHECKING FOR REQUIRED FILES")
    file_vars = get_vars(VARS_FILE, ['et'])
    if not file_vars:
        print("STAGE 0 VARS MISSING, please re-run!")
        exit(1)
    if not os.path.isfile(PARQUET_FILE_EN.format(3)):
        print("NO STAGE 3 PARQUET FILES FOUND!")
        exit(1)

    print("PREPROCESSING ESTONIAN FILE")
    lines = get_lines(RAW_FILE_ET)[:SAMPLE_SIZE]
    convert_to_parquet(lines, file_vars['et'][KEEP_PERCENTAGE_LENGTH], PARQUET_FILE_ET.format(4),
                       use_morpheme_translation,
                       MORPHEME_TRANSLATION_TABLE_ET)
    del lines
    df_et = pd.read_parquet(PARQUET_FILE_ET.format(4))
    df_en = pd.read_parquet(PARQUET_FILE_EN.format(3))
    print("PREPROCESSING COMPLETE!")

    print("FILTERING BOTH DATAFRAMES FOR COMMON LINES...")
    common_lines = set(df_en['line_no']) & set(df_et['line_no'])
    df_et = df_et.loc[df_et['line_no'].isin(common_lines)].reset_index(drop=True)
    df_et.to_parquet(PARQUET_FILE_ET.format(4))
    df_en = df_en.loc[df_en['line_no'].isin(common_lines)].reset_index(drop=True)
    df_en.to_parquet(PARQUET_FILE_EN.format(4))
    print("FILTERING COMPLETE!")

    print("STAGE 4 COMPLETE")


# STAGE 5
def run_stage_5():
    if not os.path.isfile(PARQUET_FILE_ET.format(4)):
        print("STAGE 4 FILE NOT FOUND, QUITTING NOW!")
        exit(1)
    print("STARTING STAGE 5 PRE-PROCESSING...")
    calc_word_freq(PARQUET_FILE_ET.format(4), DICT_FILE_ET)
    tokenize_file(PARQUET_FILE_ET.format(4), PARQUET_FILE_ET.format(5), DICT_FILE_ET)
    print("STAGE 5 COMPLETE!")


# STAGE 6
# TOKEN SPECIFICATION
# 0 - null token
# 1/2 - start/end tokens
# 3-7 - english unkown tokens
# 8 unknown translate token
# 9-5009 - word tokens

def add_extra_tokens(chunk_en: pd.DataFrame(), chunk_et: pd.DataFrame(), et2en_hash_map):
    print("starting process {}".format(mp.current_process()))
    print("{} - starting type conversion".format(mp.current_process()))
    chunk_en = chunk_en.drop('line_no', axis=1).astype('int64')
    chunk_et = chunk_et.drop('line_no', axis=1).astype('int64')
    print("{} - offsetting null variable".format(mp.current_process()))
    offset_null = np.vectorize(lambda x: x - EXTRA_TOKENS if x == 0 else x)
    chunk_en = chunk_en.apply(offset_null)
    chunk_et = chunk_et.apply(offset_null)

    unkown_word_low_bound = 3 - EXTRA_TOKENS
    unkown_word_high_bound = 3 - EXTRA_TOKENS + NO_UNKNOWN
    print("{} - capping and translating dictionaries".format(mp.current_process()))
    update_status_number = 10
    no_rows = len(chunk_en.index)
    update_rows = [ix for ix in range(0, no_rows, no_rows // update_status_number)]
    for row_ix in range(len(chunk_et.index)):
        if update_rows and row_ix == update_rows[0]:
            print("{} - {}% complete...".format(mp.current_process(), 100 * (
                    update_status_number - len(update_rows)) // update_status_number))
            del update_rows[0]
        # get all of the english unknown hashes
        english_unknown_hashes = list(filter(lambda x: x > WORD_CAP, chunk_en.iloc[row_ix]))

        # cap and translate estonian unknown words
        for col_ix in range(len(chunk_et.columns)):
            cell = chunk_et.iloc[row_ix, col_ix]
            if cell > WORD_CAP:
                if cell in et2en_hash_map and et2en_hash_map[cell] in english_unknown_hashes:
                    hash_index = english_unknown_hashes.index(et2en_hash_map[cell])
                    chunk_et.iloc[row_ix, col_ix] = hash_index - NO_UNKNOWN
                else:
                    chunk_et.iloc[row_ix, col_ix] = 0
            if cell == -1 * EXTRA_TOKENS or col_ix == len(chunk_et.columns) - 1:
                chunk_et.iloc[row_ix, col_ix] = 2 - EXTRA_TOKENS
                break

        # cap and translate english words
        current_unknown_word = unkown_word_low_bound
        for col_ix in range(len(chunk_en.columns)):
            cell = chunk_en.iloc[row_ix, col_ix]
            if cell > WORD_CAP:
                if current_unknown_word > unkown_word_high_bound:
                    chunk_en.iloc[row_ix, col_ix] = 0
                else:
                    chunk_en.iloc[row_ix, col_ix] = current_unknown_word
                    current_unknown_word += 1
                if cell == -1 * EXTRA_TOKENS or col_ix == len(chunk_en.columns) - 1:
                    break
    print("{} - offsetting full dictionaries and reverting types".format(mp.current_process()))
    offset_all = np.vectorize(lambda x: x + EXTRA_TOKENS)
    chunk_en = chunk_en.apply(offset_all).astype('uint16')
    chunk_et = chunk_et.apply(offset_all).astype('uint16')
    chunk_et.insert(0, 'start_token', 1)
    print("{} - process complete!".format(mp.current_process()))
    return chunk_en, chunk_et


def run_stage_6():
    print("RUNNING STAGE 6")
    files_required_exist = all(os.path.isfile(path) for path in
                               [PARQUET_FILE_ET.format(5), PARQUET_FILE_EN.format(4), DICT_FILE_EN, DICT_FILE_ET])
    if not files_required_exist:
        print("ONE OR MORE STAGE PREVIOUS STAGE FILES MISSING!")
        exit(1)

    print("LOADING FILES")
    df_en_chunks = load_parquet_in_chunks(PARQUET_FILE_EN.format(4), STAGE_6_CHUNK_SIZE, SAMPLE_SIZE)
    df_et_chunks = load_parquet_in_chunks(PARQUET_FILE_ET.format(5), STAGE_6_CHUNK_SIZE, SAMPLE_SIZE)
    dict_en = pd.read_csv(DICT_FILE_EN, index_col=0, keep_default_na=False)
    dict_et = pd.read_csv(DICT_FILE_ET, index_col=0, keep_default_na=False)

    print("dictionaries have been split into {} chunks.".format(len(df_en_chunks)))

    print("GENERATING HASH MAPS")
    # this shows which english hashes are the same as estonian hashes
    enet_hashmap = dict()
    for en_word in dict_en.index:
        if en_word in dict_et.index:
            enet_hashmap[min(dict_en.at[en_word, 'hash'])] = dict_et.at[en_word, 'hash']
    eten_hashmap = dict(zip(enet_hashmap.keys(), enet_hashmap.values()))
    print("SAVING HASH MAP")
    pd.Series(enet_hashmap).to_csv(HASH_MAPPING, index_label='en_hash')

    print("CAPPING AND CONVERTING CHUNKS!")
    with mp.Pool(THREAD_POOL_SIZE) as p:
        capped_chunks = p.starmap(add_extra_tokens,
                                  ((df_en_chunks[i], df_et_chunks[i], eten_hashmap) for i in range(len(df_en_chunks))))
    en_capped_chunks, et_capped_chunks = zip(*capped_chunks)
    del capped_chunks
    df_en_capped = pd.concat(en_capped_chunks)
    del en_capped_chunks
    df_et_capped = pd.concat(et_capped_chunks)
    del et_capped_chunks

    print("SAVING FILES")
    df_en_capped.to_parquet(PARQUET_FILE_EN.format(6))
    df_et_capped.to_parquet(PARQUET_FILE_ET.format(6))


# MORPHEME GENERATION
def get_next_letter_point(words, start_word_ix, end_word_ix, letter_ix):
    """performs bst search to find where the letters are switching"""
    start_word, end_word = words[start_word_ix], words[end_word_ix]
    lower_letter, upper_letter = start_word[letter_ix], end_word[letter_ix]
    if lower_letter == upper_letter:
        return False
    lower_ix, upper_ix = start_word_ix, end_word_ix
    while lower_ix != upper_ix - 1:
        current_ix = (lower_ix + upper_ix) // 2
        current_letter = words[current_ix][letter_ix]
        if current_letter == lower_letter:
            lower_ix = current_ix
        else:
            upper_ix = current_ix
    return upper_ix


def get_root_postfix_scores(words, start_word_ix, end_word_ix, letter_ix):
    """divide and conquer algorithm to split all root blocks and rank them accordingly
    :returns: counter(root_scores), counter(postfix_scores)"""
    if start_word_ix == end_word_ix:  # base case
        return Counter(), Counter()

    root_scores = Counter()
    postfix_scores = Counter()

    if letter_ix > MIN_ROOT_LENGTH:
        score = letter_ix * (end_word_ix - start_word_ix + 1)
        root_scores[words[start_word_ix][:letter_ix]] = score
        for word_ix in range(start_word_ix, end_word_ix + 1):
            if letter_ix < len(words[word_ix]) - 1:
                postfix_scores[words[word_ix][letter_ix + 1:]] += score

    # adjustment if the letter checked the end of the word
    if letter_ix >= len(words[start_word_ix]) and start_word_ix != end_word_ix:
        start_word_ix += 1
        if start_word_ix == end_word_ix:  # base case 2
            return root_scores, postfix_scores

    # inductive case
    prev_word_ix = start_word_ix
    next_word_ix = get_next_letter_point(words, start_word_ix, end_word_ix, letter_ix)
    while next_word_ix:
        new_root_scores, new_postfix_scores = get_root_postfix_scores(words, prev_word_ix, next_word_ix - 1,
                                                                      letter_ix + 1)
        root_scores += new_root_scores
        postfix_scores += new_postfix_scores
        prev_word_ix = next_word_ix
        next_word_ix = get_next_letter_point(words, prev_word_ix, end_word_ix, letter_ix)
    return root_scores, postfix_scores


def find_postfix(word, root, postfixes):
    if word.find(root) == 0:
        word_potential_postfix = word[len(root):]
        if not word_potential_postfix or word_potential_postfix in postfixes:
            return word_potential_postfix
    return False


def match_root_postfix(word: str, roots, postfixes, start_ix=None, end_ix=None, letter_ix=0):
    found_roots = []
    found_postfixes = []
    if letter_ix >= len(word):  # base case 1
        return found_roots, found_postfixes
    if start_ix is None:
        start_ix = 0
    if end_ix is None:
        end_ix = len(roots) - 1

    # get the first and last matching on current letter
    word_letter = word[letter_ix]
    l_ix, r_ix, cl_ix, cr_ix = start_ix, end_ix, None, None

    if len(roots[l_ix]) <= letter_ix:  # adjust in case word has no next letters
        postfix = find_postfix(word, roots[l_ix], postfixes)
        if postfix != False:
            found_roots.append(roots[l_ix])
            found_postfixes.append(postfix)
        l_ix += 1

    # adjust in case the first or last word contains the searched letter
    if roots[l_ix][letter_ix] == word_letter:
        cl_ix = cr_ix = l_ix
        l_ix -= 1
    if roots[r_ix][letter_ix] == word_letter:
        cr_ix = r_ix
        if cl_ix is None:
            cl_ix = cr_ix
        r_ix += 1

    while (not (l_ix + 1 == cl_ix and cr_ix == r_ix - 1)) and l_ix + 1 != r_ix and l_ix != r_ix:
        if cl_ix is None:
            current_ix = (l_ix + r_ix) // 2
            current_letter = roots[current_ix][letter_ix]
            if current_letter == word_letter:
                cl_ix = cr_ix = current_ix
            elif current_letter < word_letter:
                l_ix = current_ix
            else:
                r_ix = current_ix
        elif l_ix + 1 == cl_ix:
            current_ix = (cr_ix + r_ix) // 2
            current_letter = roots[current_ix][letter_ix]
            if current_letter == word_letter:
                cr_ix = current_ix
            else:
                r_ix = current_ix
        else:
            current_ix = (l_ix + cl_ix) // 2
            current_letter = roots[current_ix][letter_ix]
            if current_letter == word_letter:
                cl_ix = current_ix
            else:
                l_ix = current_ix

    if l_ix + 1 == r_ix or l_ix == r_ix:  # base case 2
        check_roots = [roots[l_ix]]
        if l_ix + 1 == r_ix:
            check_roots.append(roots[r_ix])
        for check_root in check_roots:
            postfix = find_postfix(word, check_root, postfixes)
            if postfix != False:
                found_roots.append(check_root)
                found_postfixes.append(postfix)
            return found_roots, found_postfixes
        return found_roots, found_postfixes
    if cl_ix == cr_ix:  # base case 3
        check_root = roots[cl_ix]
        postfix = find_postfix(word, check_root, postfixes)
        if postfix != False:
            found_roots.append(check_root)
            found_postfixes.append(postfix)
        return found_roots, found_postfixes
    new_roots, new_postfixes = match_root_postfix(word, roots, postfixes, cl_ix, cr_ix, letter_ix + 1)
    postfix = find_postfix(word, roots[cl_ix], postfixes)
    if postfix != False and postfix not in new_postfixes:
        found_roots.append(roots[cl_ix])
        found_postfixes.append(postfix)
    return found_roots + new_roots, found_postfixes + new_postfixes


def generate_morphemes(vocabulary_filepath, morpheme_translation_table_path):
    print("RUNNING MORPHEME GENERATION STAGE...")
    dict_file = pd.read_csv(vocabulary_filepath, index_col=0, keep_default_na=False).iloc[1:SAMPLE_SIZE]
    words = sorted(dict_file.index)

    print("generating root and postfix scores")
    root_scores, postfix_scores = get_root_postfix_scores(words, 0, len(words) - 1, 0)
    sorted_root_scores = pd.Series(root_scores).sort_index()
    sorted_postfix_scores = pd.Series(postfix_scores).sort_values(ascending=False)

    print("filtering roots and postfixes")
    filtered_postfix_scores = sorted_postfix_scores.iloc[:POSTFIX_KEEP_NUMBER].sort_index()

    print("matching words to the roots and postfixes")
    word_roots, word_postfixes = {}, {}
    for word in words:
        found_roots, found_postfixes = match_root_postfix(word, sorted_root_scores.index, filtered_postfix_scores.index)
        if found_roots:
            word_roots[word] = found_roots
            word_postfixes[word] = found_postfixes

    print("frequency scoring each root...")
    root_scores_adjusted = Counter()
    for word in word_roots:
        for root in word_roots[word]:
            root_scores_adjusted[root] += 1

    print("eliminating conflicting roots")
    for word in word_roots:
        if len(word_roots[word]) > 1:
            max_ix = 0
            max_root = None
            max_score = 0
            for ix, root in enumerate(word_roots[word]):
                score = root_scores_adjusted[root]
                if score > max_score or (score == max_score and len(root) < len(max_root)):
                    max_ix = ix
                    max_root = root
                    max_score = score
            word_roots[word] = word_roots[word][max_ix]
            word_postfixes[word] = word_postfixes[word][max_ix]
        else:
            word_roots[word] = word_roots[word][0]
            word_postfixes[word] = word_postfixes[word][0]

    print("generating translation table")
    translation_table = pd.DataFrame(
        {'word': list(word_roots.keys()), 'root': list(word_roots.values()), 'postfix': list(word_postfixes.values())})

    print("SAVING TEMPORARY FILES...")
    translation_table.to_csv(morpheme_translation_table_path, index=False)

    print("MORPHEME GENERATION COMPLETE!")


# TRAINING
def gen_xy(x_filepath, y_filepath, validate=False):
    """
    batch generator, generates batches of data data and y labels for preprocessing
    :param x_filepath: filepath to encoder tokens
    :param y_filepath: filepath to decoder tokens
    :param validate: if validate is true it will use small subset of the training set to validate
    :return: [x_enc_tokens, x_dec_tokens], y_onehot_vectors
    """
    X_full: np.ndarray = pd.read_parquet(x_filepath).values
    Y_full: np.ndarray = pd.read_parquet(y_filepath).values
    no_rows = X_full.shape[0]
    y_len = Y_full.shape[1]
    start_ix = int(no_rows * VALIDATE_OFFSET)
    end_ix = int(no_rows * (VALIDATE_OFFSET + VALIDATE_SET))
    if validate:
        # get the validation set
        X = X_full[start_ix:end_ix]
        Y = Y_full[start_ix:end_ix]
        no_rows = X.shape[0]
    else:
        # get the training set
        X = np.concatenate((X_full[:start_ix], X_full[end_ix:]))
        Y = np.concatenate((Y_full[:start_ix], Y_full[end_ix:]))
        no_rows = X.shape[0]
    current_ix = 0
    while True:
        x_enc = X[current_ix:current_ix + BATCH_SIZE]
        x_dec = Y[current_ix:current_ix + BATCH_SIZE]
        y = np.zeros((BATCH_SIZE, y_len, NO_TOKENS))
        token_rows, token_cols = np.where(x_dec != 0)
        token_dims = x_dec[token_rows, token_cols]
        y[token_rows, token_cols, token_dims] = 1
        if validate:
            current_ix = int((current_ix + BATCH_SIZE) % (no_rows - (no_rows * VALIDATE_OFFSET)))
        else:
            current_ix = int((current_ix + BATCH_SIZE) % (no_rows - BATCH_SIZE))
        yield [x_enc, x_dec[:, :-1]], y[:, 1:, :]


def train_model(x_filepath, y_filepath):
    # Define an input sequence and process it.
    encoder_inputs = Input(shape=(None,))
    x = Embedding(NO_TOKENS, LATENT_DIM)(encoder_inputs)
    # data= LSTM(LATENT_DIM,return_sequences=True)(data)
    x, state_h, state_c = LSTM(LATENT_DIM, return_state=True)(x)
    #
    encoder_states = [state_h, state_c]

    # Set up the decoder, using `encoder_states` as initial state.
    decoder_inputs = Input(shape=(None,))
    x = Embedding(NO_TOKENS, LATENT_DIM)(decoder_inputs)
    x, _, _ = LSTM(LATENT_DIM, return_sequences=True, return_state=True)(x, initial_state=encoder_states)
    # data = LSTM(LATENT_DIM,return_sequences=True)(data)
    decoder_outputs = Dense(NO_TOKENS, activation='softmax')(x)

    # Define the model that will turn
    # `encoder_input_data` & `decoder_input_data` into `decoder_target_data`
    model = Model([encoder_inputs, decoder_inputs], decoder_outputs)

    # Compile & run training
    sgd = optimizers.rmsprop(clipvalue=0.5)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])
    checkpoint = ModelCheckpoint(TEMP_MODEL_WEIGHTS_FILEPATH, monitor='val_acc', verbose=1, save_best_only=True,
                                 mode='max')
    callbacks_list = [checkpoint]
    model.summary()
    model.fit_generator(gen_xy(x_filepath, y_filepath), STEPS_PER_EPOCH, EPOCH_NO,
                        validation_data=gen_xy(x_filepath, y_filepath, True), validation_steps=20, verbose=1,
                        callbacks=callbacks_list)
    model.save(FINAL_MODEL_FILEPATH)

try:
    # DECODING
    model = load_model(FINAL_MODEL_FILEPATH)

    encoder_inputs = model.input[0]  # input_1
    encoder_outputs, state_h_enc, state_c_enc = model.layers[4].output  # lstm_1
    encoder_states = [state_h_enc, state_c_enc]
    encoder_model = Model(encoder_inputs, encoder_states)

    decoder_inputs = model.input[1]  # input_2
    decoder_state_input_h = Input(shape=(LATENT_DIM,), name='input_3')
    decoder_state_input_c = Input(shape=(LATENT_DIM,), name='input_4')
    decoder_states_inputs = [decoder_state_input_h, decoder_state_input_c]
    decoder_embedding = model.layers[3]  # decoder_embedding
    x = decoder_embedding(decoder_inputs)
    # data = Embedding(NO_TOKENS, LATENT_DIM, weights=decoder_embedding.get_weights()[0], trainable=False)(decoder_inputs)
    decoder_lstm = model.layers[5]  # lstm_2
    decoder_lstm.return_state = True
    decoder_outputs, state_h_dec, state_c_dec = decoder_lstm(x, initial_state=decoder_states_inputs)
    decoder_states = [state_h_dec, state_c_dec]
    decoder_dense = model.layers[6]
    decoder_outputs = decoder_dense(decoder_outputs)
    decoder_model = Model([decoder_inputs] + decoder_states_inputs, [decoder_outputs] + decoder_states)

    df_en_word2token = pd.read_csv(DICT_FILE_EN, index_col=0)
    df_et_token2word = pd.read_csv(DICT_FILE_ET, index_col='hash')
    translate_df = pd.read_csv(MORPHEME_TRANSLATION_TABLE_EN, index_col='word')
except:
    print("No final model weights found, if this is the first run please run preprocessing and then train the model!")

MAX_TRANSLATE_LENGTH = 40


def get_tokens(sentence, dict_file):
    words = preprep_line(sentence, translate_df)
    tokens = [dict_file.at[word, 'hash'] if word in dict_file.index else WORD_CAP + 1 for word in words]
    current_unknown_word = 3
    for ix in range(len(tokens)):
        if tokens[ix] > WORD_CAP:
            tokens[ix] = current_unknown_word
            current_unknown_word = min(current_unknown_word + 1, 7)
        else:
            tokens[ix] += EXTRA_TOKENS
    return tokens


def get_sentence(tokens, dict_file):
    new_tokens = []
    for token in tokens:
        if token == 5:
            break
        new_tokens.append(token - EXTRA_TOKENS)
    # TODO put morphemes back into individual words
    return ' '.join([dict_file.at[token, 'Unnamed: 0'] for token in new_tokens if token > EXTRA_TOKENS])


def decode_sequence(input_seq):
    # use the model to get a list of tokens
    tokens = np.array(get_tokens(input_seq, df_en_word2token))
    
    # generate token prediction
    states_value = encoder_model.predict(tokens)
    sampled_token_index = 1
    translated_tokens = []
    while True:
        output_tokens, h, c = decoder_model.predict([np.array([[sampled_token_index]])] + states_value)

        # Sample a token
        sampled_token_index = np.argmax(output_tokens[0, -1, 9:])
        sampled_token_max_p = np.max(output_tokens[0, -1, 9:])
        end_token_p = output_tokens[0, -1, 5]

        if end_token_p > sampled_token_max_p or len(translated_tokens) > MAX_TRANSLATE_LENGTH:
            break
        translated_tokens.append(sampled_token_index)
        # Update states
        states_value = [h, c]

    # decode the tokens back to a string
    sentence = get_sentence(translated_tokens, df_et_token2word)

    return sentence


# RUNNING THE SCRIPT
runner_map = {1: run_stage_1,
              2: run_stage_2,
              3: run_stage_3,
              4: run_stage_4,
              5: run_stage_5,
              6: run_stage_6}

STAGES_WITH_MORPHEME_GENERATION = [1, 2, 4]


def run_stage(stage, use_morpheme_translation_table=None):
    if stage in STAGES_WITH_MORPHEME_GENERATION and use_morpheme_translation_table is not None:
        return runner_map[stage](use_morpheme_translation_table)
    return runner_map[stage]()


def run_complete_preprocessing_pipeline():
    response = input("this is going to reset your data, are you sure? Y/n")
    if response.lower() == 'y':
        print("BACKING UP OLD FILES...")
        if not os.path.isdir(BACKUP_DIR):
            os.makedirs(BACKUP_DIR)
        datetime_str = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        os.makedirs(os.path.join(BACKUP_DIR, datetime_str))
        move_files = [f for f in os.listdir(DATA_DIR)
                      if os.path.isfile(os.path.join(DATA_DIR, f)) and f not in RAW_FILES]
        for move_file in move_files:
            os.rename(os.path.join(DATA_DIR, move_file), os.path.join(BACKUP_DIR, datetime_str, move_file))

        print("GENERATING VOCABULARIES (STAGES 1-5)...")
        gen_vocabulary_stages = [1, 2, 3, 4, 5]
        for stage in gen_vocabulary_stages:
            run_stage(stage, use_morpheme_translation_table=False)

        print("CREATING MORPHEME TRANSLATION TABLES")
        print("ENGLISH FILE...")
        generate_morphemes(DICT_FILE_EN, MORPHEME_TRANSLATION_TABLE_EN)
        print("ESTONIAN FILE...")
        generate_morphemes(DICT_FILE_ET, MORPHEME_TRANSLATION_TABLE_ET)

        print("BACKING UP OLD VOCABULARY FILES...")
        os.rename(DICT_FILE_EN, os.path.join(DATA_DIR, 'en_dict_old.csv'))
        os.rename(DICT_FILE_ET, os.path.join(DATA_DIR, 'et_dict_old.csv'))

        print("RUNNING FULL PIPELINE WITH MORPHEME TRANSLATION (STAGES 1-6)...")
        pipeline_with_morpheme_translation_steps = [1, 2, 3, 4, 5, 6]
        for stage in pipeline_with_morpheme_translation_steps:
            run_stage(stage, use_morpheme_translation_table=True)

        print("PREPROCESSING COMPLETE!")


def run_model_training():
    if not all(map(os.path.isfile, [PARQUET_FILE_EN.format(6), PARQUET_FILE_ET.format(6)])):
        print("preprocessed files missing! Please run all of the preprocessing stages first!")
        exit(1)
    print("TRAINING THE MODEL...")
    train_model(PARQUET_FILE_EN.format(6), PARQUET_FILE_ET.format(6))
    print("TRAINING COMPLETE!")


def decode(input_sentence):
    if not os.path.isfile(FINAL_MODEL_FILEPATH):
        print("trained model not found! Please train the model first!")
        exit(1)
    #from ENGEST.training import decode
    return decode_sequence(input_sentence)


for stage in sorted(MANUAL_RUN_STAGES):
    run_stage(stage)
    
# To train, first run run_complete_preprocessing_pipeline() once to generate the necessary files. Then run_model_training()

print(decode("European Court of Justice"))