
# coding: utf-8

# In[1]:

from __future__ import print_function
import keras,glob
from keras.datasets import cifar100
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten,GaussianDropout
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization,GlobalAveragePooling2D, Reshape, multiply, Permute,UpSampling2D
from keras import optimizers
import numpy as np
from keras.layers.core import Lambda
from keras import backend as K
from keras import regularizers
from keras.models import Model
from keras.layers import Input

from keras.layers import GlobalAveragePooling2D, Reshape, Dense, multiply, Permute
from keras import backend as K
import tensorflow as tf
from keras.callbacks import ModelCheckpoint

import glob

import os
from scipy.ndimage.filters import gaussian_filter
from skimage.io import imread_collection,imread
from PIL import Image,ImageDraw
import matplotlib.pyplot as plt
import cv2
import numpy as np
import pandas as pd

from skimage.draw import polygon,circle,ellipse



# In[2]:



np.random.seed(1)

(person_x_train, person_y_train), (person_x_test, person_y_test) = cifar100.load_data(label_mode='coarse')
person_x_train = person_x_train.astype('float32')
person_x_test = person_x_test.astype('float32')
#print(person_x_train)

person_y_train = keras.utils.to_categorical(person_y_train, 20)#[:,14]
person_y_test = keras.utils.to_categorical(person_y_test, 20)


PERSON_TRAINING=False                                   # Change to train!
FACE_TRAINING=False



######PREPROCESSING#########################
print("Started Data Processing")
##PERSON##

def adjust_CIFAR_proportions(imgs,targets):
    
    people=[]
    people_labels=[]
    for beep,v in enumerate(targets):
        if targets[beep][14]==1:
            #print(person_x_train[beep])
            people.append(imgs[beep])
            people_labels.append(targets[beep])
    
    #Make proportion of people to other classes 1:1
    #initially prop 1:19, add 18 copies of person set, shuffle        
        
    people=np.array(people*18)     
    people_labels=np.array(people_labels*18)  
        
   
    imgs=np.concatenate((imgs,people))      
    targets=np.concatenate((targets,people_labels))

#shuffle
    index=np.arange(len(targets))
    np.random.shuffle(index)

    imgs,targets=imgs[index],targets[index]

    
    targets_single=np.array([labels[14]==1 for labels in targets]).astype(int)

    return(imgs,targets_single)
#person_x_train
(person_x_train,person_y_train)=adjust_CIFAR_proportions(person_x_train,person_y_train)
(person_x_test, person_y_test)=adjust_CIFAR_proportions(person_x_test, person_y_test)

#plt.imshow(person_x_train[-5]/255)
#plt.show()


# In[3]:



#no_people=0
#for beep,v in enumerate(person_y_train):
#    if person_y_train[beep]==1:
#        #print(person_x_train[beep])
#        no_people+=1
#        #people.append(person_x_train[beep])

#person_y_train_single=np.array([labels[14]==1 for labels in person_y_train]).astype(int)
#person_y_train[0]






######## FACE ################ FACE ############### FACE #########

low_imgs,targets=[],[]

#300W Data

img_paths=glob.glob("../DATA/300W/01_Indoor/*.png")+glob.glob("../DATA/300W/02_Outdoor/*.png")

for path in img_paths:
    #print(path)
    pts_path=path[:-3]+"pts"
    img=imread(path)#,iscolor='CV_LOAD_IMAGE_COLOR')

    if len(img.shape)==2:   ## Make sure RGB
        img = np.repeat(img[:, :, np.newaxis], 3, axis=2)
        
    masked_img=img

    points_raw=pd.read_csv(pts_path,delim_whitespace=True).dropna()
    points=np.array(points_raw)[1:].astype(float)
    assert (len(points)==np.array(points_raw)[0][1] and len(points)==68)
    points=points-np.ones(points.shape)
    (y,x)=points[34]
    r=np.linalg.norm(np.array(points[34])-np.array(points[38]))*1.5
    rr, cc = circle(x, y, r)
   
    polypoints=points
    polypoints[17:27]=polypoints[17:27][::-1] #Reverse section of points so polygon draws correctly
    
    label=np.zeros(img.shape)
    
    rr, cc = polygon(polypoints.T[1][:27], polypoints.T[0][:27])

    label[rr,cc]=[1.0]
    
    
    
    for point in points:
        (y,x)=point# 

        x=int(float(x))
        y=int(float(y))

        r=10
        rr, cc = circle(x, y, r)

    low_imgs.append(cv2.resize(img, dsize=(32, 32), interpolation=cv2.INTER_CUBIC))
    targets.append(cv2.resize(label, dsize=(32, 32), interpolation=cv2.INTER_CUBIC))

input_imgs=np.array(low_imgs)
target_imgs=np.array(targets)
target_logits=np.array(targets).mean(axis=3)#,keepdims=True)


#HollywoodHeads Data

def get_xml_value(content,attr):
    
    return content[content.index("<"+attr+">") + len("<"+attr+">"):content.index("</"+attr+">")]

h_low_imgs,h_targets=[],[]
fname="../DATA/HollywoodHeads/Splits/train.txt"
with open(fname) as f:
    content = f.readlines()
content = [x.strip() for x in content]
np.random.shuffle(content)
for name in content[:600]:
    img=imread("../DATA/HollywoodHeads/JPEGImages/"+name+".jpeg")#,iscolor='CV_LOAD_IMAGE_COLOR')
    
    if len(img.shape)==2:   ## Make sure RGB
        img = np.repeat(img[:, :, np.newaxis], 3, axis=2)

    
    
    ## Get Points
    with open("../DATA/HollywoodHeads/"+"Annotations/"+name+".xml") as f:
        content = f.read()

    heads =[]
    for head in content.split("<name>head</name>")[1:]:
        box={}

        box['xmin']=int(round(float(get_xml_value(head,"xmin"))))-1
        box['xmax']=int(round(float(get_xml_value(head,"xmax"))))-1
        box['ymin']=int(round(float(get_xml_value(head,"ymin"))))-1
        box['ymax']=int(round(float(get_xml_value(head,"ymax"))))-1
        heads.append(box)
        
    label=np.zeros(img.shape)  

    for box in heads:
        rr, cc = ellipse(np.mean([box['ymin'],box['ymax']]),np.mean([box['xmin'],box['xmax']]), (box['ymax']-box['ymin'])/2.0, (box['xmax']-box['xmin'])/2.0)
        label[rr,cc]=[1]
        
    h_low_imgs.append(cv2.resize(img, dsize=(32, 32), interpolation=cv2.INTER_CUBIC))
    h_targets.append(cv2.resize(label, dsize=(32, 32), interpolation=cv2.INTER_CUBIC))
        


h_input_imgs=np.array(h_low_imgs)
h_input_imgs=h_input_imgs-np.mean(h_input_imgs,axis=(0,1,2,3))
h_input_imgs=h_input_imgs/np.std(h_input_imgs, axis=(0, 1, 2, 3))
h_target_imgs=np.array(h_targets)
h_target_logits=np.array(h_targets).mean(axis=3)


input_imgs=input_imgs-np.mean(input_imgs,axis=(0,1,2,3))
input_imgs=input_imgs/np.std(input_imgs, axis=(0, 1, 2, 3))


input_imgs=np.concatenate((input_imgs,h_input_imgs))
target_imgs=np.concatenate((target_imgs,h_target_imgs))
target_logits=np.concatenate((target_logits,h_target_logits))

index=np.arange(len(input_imgs))
np.random.shuffle(index)

input_imgs=input_imgs[index]
target_imgs=target_imgs[index]
target_logits=target_logits[index]

####################################################################

print("Finished Data Processing")

# In[4]:

def squeeze_excite(input, ratio=16):

    init = input
    filters = init._keras_shape[-1]
    se_shape = (1, 1, filters)

    se = GlobalAveragePooling2D()(init)
    se = Reshape(se_shape)(se)
    se = Dense(filters // ratio, activation='relu', kernel_initializer='he_normal', use_bias=False)(se)
    se = Dense(filters, activation='sigmoid', kernel_initializer='he_normal', use_bias=False)(se)

    if K.image_data_format() == 'channels_first':
        se = Permute((3, 1, 2))(se)

    x = multiply([init, se])
    return x


class person_detect:
    def __init__(self,train):
        
        self.batch_size=128
        
        self.num_classes = 1
        self.l2_param = 0.0005
        self.x_shape = [32,32,3]

        self.model = self.build_model()
        if not train:
            self.model.load_weights('best_weights_person.hdf5')


    def build_model(self):
       
        visible = Input(shape=self.x_shape)  
        
        l2_param = self.l2_param
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(visible)
        h=Activation('relu')(h)
        o=BatchNormalization()(h)
        h=GaussianDropout(0.3)(h)
        
        h=squeeze_excite(h)
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        
        h=keras.layers.add([o,h])
        
        m=MaxPooling2D(pool_size=(2, 2))(h)

        massive_skip=MaxPooling2D(pool_size=(4, 4))(h)
        massive_skip=Conv2D(256, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(massive_skip)
        
        massive_skip=Activation('relu')(massive_skip)
        massive_skip=BatchNormalization()(massive_skip)
        
        
        
        
        h=Conv2D(128, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(m)
        h=Activation('relu')(h)
        o=BatchNormalization()(h)
        h=GaussianDropout(0.4)(o)
        
        h=squeeze_excite(h)
        
        
        h=Conv2D(128, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        
        h=keras.layers.add([o,h])
        
        h=MaxPooling2D(pool_size=(2, 2))(h)

        
        
        
        
        
        
                
        h=Conv2D(256, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        o=BatchNormalization()(h)
        h=GaussianDropout(0.4)(o)
        
        h=squeeze_excite(h)
        
        
        h=Conv2D(256, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        
        h=keras.layers.add([massive_skip,o,h])
        
        
        h=MaxPooling2D(pool_size=(2, 2))(h)


        
        
        h=Conv2D(512, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=GaussianDropout(0.4)(h)
        
        h=squeeze_excite(h)
        
        h=GlobalAveragePooling2D()(h)
        
        h=Dense(512,kernel_regularizer=regularizers.l2(l2_param))(h)
        
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=GaussianDropout(0.5)(h)
        
        h=Dense(512,kernel_regularizer=regularizers.l2(l2_param))(h)
        
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=GaussianDropout(0.5)(h)
        
        h=Dense(self.num_classes,kernel_regularizer=regularizers.l2(l2_param))(h)
        
        h=Activation('sigmoid')(h)
        
        model = Model(inputs=visible, outputs=h)
        
        model.summary()


        return model


    def normalise(self,person_x_train,person_x_test):
        mean = np.mean(person_x_train,axis=(0,1,2,3))
        std = np.std(person_x_train, axis=(0, 1, 2, 3))
        print(mean)
        print(std)
        person_x_train = (person_x_train-mean)/(std+1e-7)
        person_x_test = (person_x_test-mean)/(std+1e-7)
        return person_x_train, person_x_test

    def normalise_production(self,x):

        mean = 120.56209
        std = 69.90403
        return (x-mean)/(std+1e-7)

    def predict(self,x,normalise=True):#,batch_size=50):
        if normalise:
            x = self.normalise_production(x)
        return self.model.predict(x)#,self.batch_size)

    def train(self,model,imgs,target,test_imgs,test_target):

        #training parameters
        batch_size = self.batch_size
        #
        maxepoches = 250
        learning_rate = 0.1
        lr_decay = 1e-6
        lr_drop = 20


        person_x_train, person_x_test = self.normalise(imgs, test_imgs)
        person_y_train, person_y_test = target,test_target


        #data augmentation
        datagen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=15,  # randomly rotate images in the range (degrees, 0 to 180)
            width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=False)  # randomly flip images
        # (std, mean, and principal components if ZCA whitening is applied).
        datagen.fit(person_x_train)

        adam=keras.optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.00, amsgrad=False)
              
        model.compile(loss='binary_crossentropy', optimizer=adam,metrics=['accuracy'])

        check_pointer = ModelCheckpoint('best_weights_person.hdf5', 
              save_best_only=True)
    
    
        model.fit_generator(datagen.flow(person_x_train, person_y_train,
              batch_size=batch_size),
              epochs=maxepoches,
              #batch_size=batch_size,
              callbacks=[check_pointer],
              verbose=1,
              #validation_split=0.1,   
              validation_data=(person_x_test, person_y_test))
    
        self.model.load_weights('best_weights_person.hdf5')
        #model.save_weights('cifar100vgg.h5')
        
        
        #return model


class face_detect:
    def __init__(self,train=True):
        
        self.l2_param = 0.0005
        self.x_shape = [32,32,3]

        self.model = self.U_net_model()
        if not train:
            self.model.load_weights('best_weights_face.hdf5')


    def U_net_model(self):

        l2_param=self.l2_param
        visible = Input(shape=self.x_shape)  
        
        h=BatchNormalization()(visible)
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        o=BatchNormalization()(h)
        h=Dropout(0.3)(h)
        
        h=squeeze_excite(h)
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        early=BatchNormalization()(h)

        max1=MaxPooling2D(pool_size=(2, 2))(early)


        
        
        h=Conv2D(128, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(max1)
        h=Activation('relu')(h)
        o=BatchNormalization()(h)
        h=Dropout(0.4)(o)
        
        h=squeeze_excite(h)
        
        
        h=Conv2D(128, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
 
        max2=MaxPooling2D(pool_size=(2, 2))(h)


                
        h=Conv2D(256, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(max2)
        h=Activation('relu')(h)
        o=BatchNormalization()(h)
        h=Dropout(0.4)(o)
        
        h=squeeze_excite(h)
        
        
        h=Conv2D(256, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)

        max3=MaxPooling2D(pool_size=(2, 2))(h)

  
        
        h=Conv2D(512, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(max3)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        h=Conv2D(256, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        h=squeeze_excite(h)
        
        h=Conv2D(128, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        
        up1=UpSampling2D(size = (2,2))(h)
        
        join=keras.layers.add([max2,up1])
        
        
        h=Conv2D(128, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(join)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        h=Conv2D(128, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        
        
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        up2=UpSampling2D(size = (2,2))(h)
        
        join=keras.layers.add([max1,up2])
        
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(join)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        
        
        
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        up3=UpSampling2D(size = (2,2))(h)
        
        join=keras.layers.add([early,up3])
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(join)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        h=Conv2D(64, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Activation('relu')(h)
        h=BatchNormalization()(h)
        h=Dropout(0.4)(h)
        
        
        h=Conv2D(1, (3, 3), padding='same',
                         kernel_regularizer=regularizers.l2(l2_param))(h)
        h=Reshape((32,32))(h)

        logits=Activation('sigmoid')(h)

        
        model = Model(inputs=visible, outputs=logits)

        model.summary()

        return model


    def normalise(self,X_train,X_test):
        mean = np.mean(X_train,axis=(0,1,2,3))
        std = np.std(X_train, axis=(0, 1, 2, 3))
        print(mean)
        print(std)
        X_train = (X_train-mean)/(std+1e-7)
        X_test = (X_test-mean)/(std+1e-7)
        return X_train, X_test

    def normalise_production(self,x):
        mean = np.mean(x,axis=(0,1,2,3))#121.936 

        std = np.std(x, axis=(0, 1, 2, 3))#68.389
        return (x-mean)/(std+1e-7)

    def predict(self,x,normalise=True,batch_size=50):
        if normalise:
            #pass
            x = self.normalise_production(x)
        #x=x#/255.0-0.5
        return self.model.predict(x,batch_size)
    def postprocess_filter(self,predictions):
        filters=[]
        for i,v in enumerate(predictions):
            b=np.repeat(v[:, :, np.newaxis], 3, axis=2)
            exaggerate=b**4
            exaggerate=gaussian_filter(exaggerate, sigma=4.5)
            exaggerate=exaggerate/np.max(exaggerate)
            exaggerate=(exaggerate-np.mean(exaggerate))*2*2

            
            exaggerate[exaggerate > 0.5] = 1
            exaggerate[exaggerate < 0.5] = 0
            filters.append(exaggerate)

            return np.array(filters)
    def predict_and_postprocess(self,x,normalise=True,batch_size=50):
        return self.postprocess_filter(self.predict(x,normalise,batch_size)) 
    def show_faces(self,x):
        return x*self.predict_and_postprocess(x,True)/255
    def train(self,model,imgs,targets):

        #training parameters
        batch_size = 128
        maxepoches = 250
        learning_rate = 0.1
        lr_decay = 1e-6
        lr_drop = 20

        x_train, y_train, x_test, y_test=imgs[:-10],targets[:-10],imgs[-10:],targets[-10:]

        x_train, x_test = self.normalise(x_train, x_test)
        

        #data augmentation
        datagen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=15,  # randomly rotate images in the range (degrees, 0 to 180)
            width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=False)  # randomly flip images
        adam=keras.optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
              
        model.compile(loss='binary_crossentropy', optimizer=adam,metrics=['accuracy'])


        check_pointer = ModelCheckpoint('best_weights_face.hdf5', 
        save_best_only=True)

        model.fit(x_train,y_train,
              batch_size=batch_size,
              epochs=maxepoches,
              verbose=1,
              callbacks=[check_pointer],
              validation_split=0.1)

        return model



if __name__ == '__main__':
    
    

    x_final_test,y_final_test,x_val, y_val=person_x_test[1000:], person_y_test[1000:],person_x_test[:1000], person_y_test[:1000]
    
    person_model = person_detect(PERSON_TRAINING)
    if PERSON_TRAINING:
        person_model.train(person_model.model,person_x_train,person_y_train,x_val, y_val)


    face_model = face_detect(FACE_TRAINING)


    if FACE_TRAINING:
        face_model.train(face_model.model,input_imgs,target_logits)

    
    
    ## Person Classification

    person_predictions=person_model.predict(x_final_test)
    

    correctly_guessed=0
    total=0
    for i,v in enumerate(person_predictions):

        
        correctly_guessed+=(round(v[0])==y_final_test[i])
        total+=1
        

    print("person_accuracy: "+str(correctly_guessed/total))
    

    ## Face Recognition
    
    (x_train, y_train), (x_test, y_test) = cifar100.load_data(label_mode="coarse")
    x_test = x_test.astype('float32')
    x_train=x_train.astype('float32')
    #x_train, x_test = model.normalise(x_train, x_test)
    people_list=[]
    index_list=[]
    for beep in range(5000):
        if y_test[beep]==14:
            people_list.append(x_test[beep])
            index_list.append(beep)
            
            
    for test_face_img in np.array(people_list):
        
        #  Note that the model methods accepts and return batches
        
        f, axarr = plt.subplots(1,3)
        axarr[0].imshow(test_face_img/255)
        axarr[1].imshow(face_model.predict_and_postprocess([test_face_img])[0])

        axarr[2].imshow(face_model.show_faces([test_face_img])[0])
        
        plt.show()
